import numpy as np
import apu.apu as apu
import apu.unit.stft.wola as wola
import apu.nlt.native.psd as psd
import apu.unit.bandrouting.coupling as coupling
import apu.unit.bandrouting.distribution as distribution
import apu.unit.tracking.recursive as recursive
import apu.unit.amplification.broadband as ampbb
import apu.nlt.native.logscl as lscl



class Unit(apu.Unit):
    def __init__(self, config=dict, inherit_par=dict):
        super().__init__()
        self.cnt = 0
        self.out = np.zeros([inherit_par['onchan'],config['frame_len']])

        # fwd stft
        sub_inherit_par = {
            'sampling_rate': config['sampling_rate'],
            'frame_len': config['frame_len'],
            'nchan': inherit_par['inchan'],
        }
        self.wola_fwd = wola.WolaFwd(
            config['_sub_fwd'],
            inherit_par=sub_inherit_par)
        
        # bwd stft
        sub_inherit_par = {
            'sampling_rate': config['sampling_rate'],
            'frame_len': config['frame_len'],
            'nchan': inherit_par['onchan'],
        }
        self.wola_bwd = wola.WolaBwd(
            config['_sub_bwd'],
            inherit_par=sub_inherit_par)
            

        # parseval
        sub_inherit_par = {
            'nband': self.wola_bwd.M,
            'norm_sspec': self.wola_bwd.norm_single_sided_spectrum,
            'awin_energy': self.wola_bwd.Ewa,
        }
        self.parseval = coupling.Parseval(sub_inherit_par)
        self.powa_x = np.zeros([inherit_par['nframes'],inherit_par['inchan'],1])

        # envelope tracking
        sub_inherit_par = {
            'sampling_rate': self.wola_fwd.down_sample_freq,
            'nchan': inherit_par['inchan'],
            'nband': 1,
        }

        self.psda_x_de = np.zeros([inherit_par['nframes'],inherit_par['inchan'],1])
        self.psda_x_pre = np.zeros([inherit_par['nframes'],inherit_par['inchan'],1])
        self.psda_x_env = np.zeros([inherit_par['nframes'],inherit_par['inchan'],1])
        self.psda_x_env_avg = np.zeros([inherit_par['nframes'],inherit_par['inchan'],1])
        self.psda_x_env_pk = np.zeros([inherit_par['nframes'],inherit_par['inchan'],1])
        self.psda_x_env_vy = np.zeros([inherit_par['nframes'],inherit_par['inchan'],1])

        self.psda_x_env_slim = np.zeros([inherit_par['nframes'],inherit_par['inchan'],1])

        self.tracker_de = recursive.DualExp(
           config=config['_sub_tracker_de'],
           inherit_par=sub_inherit_par)

        self.tracker_pre = recursive.Tracker(
            config=config['_sub_tracker_pre'],
            inherit_par=sub_inherit_par)

        self.tracker_env = recursive.Tracker3SegAdaptive(
            config=config['_sub_tracker_env'],
            inherit_par=sub_inherit_par)
        
        self.tracker_env_avg = recursive.Tracker3SegAdaptive(
            config=config['_sub_tracker_env_avg'],
            inherit_par=sub_inherit_par)

        self.tracker_env_pk = recursive.Tracker3SegAdaptive(
            config=config['_sub_tracker_env_pk'],
            inherit_par=sub_inherit_par)

        self.tracker_env_vy = recursive.Tracker3SegAdaptive(
            config=config['_sub_tracker_env_vy'],
            inherit_par=sub_inherit_par)

        # dynamic amplification

        self.bbg_pta = np.zeros([inherit_par['nframes'],inherit_par['inchan'],1])
        self.bbg_osdpa = np.zeros([inherit_par['nframes'],inherit_par['inchan'],1])
        self.bbg_osdpag_post_hi = np.zeros([inherit_par['nframes'],inherit_par['inchan'],1])
        self.bbg_osdpag_post_hi_lt = np.zeros([inherit_par['nframes'],inherit_par['inchan'],1])
        self.bbg_osdpag_prio_hi = np.zeros([inherit_par['nframes'],inherit_par['inchan'],1])
        self.bbg_osdpa_post_pu_lo = np.zeros([inherit_par['nframes'],inherit_par['inchan'],1])
        self.amp_overshoot = np.zeros([inherit_par['nframes'],inherit_par['inchan'],1])

        self.pta =  ampbb.PeakTrackingBasedAmp(
            config=config['_sub_ampbb_pta'],
            inherit_par=sub_inherit_par)

        self.osdpa =  ampbb.Osdp(
            config=config['_sub_ampbb_osdp'],
            inherit_par=sub_inherit_par)

        # gain distribution
        self.distrib = distribution.Distribution(config['_sub_distrib'])
            

    def cycle(self,data=np.array) -> None:

        # fwd stft
        self.wola_fwd.cycle(x=data) 

        # parseval
        self.parseval.cycle(self.wola_fwd.X)
        self.powa_x[self.cnt,:,:] = self.parseval.y

        # envelope tracking

        self.tracker_de.cycle(lscl.pow2dB(self.parseval.y))
        self.psda_x_de[self.cnt,:,:] = self.tracker_de.y

        self.tracker_pre.cycle(self.parseval.y)
        self.psda_x_pre[self.cnt,:,:] = self.tracker_pre.y

        self.tracker_env.cycle(self.tracker_pre.y)
        self.psda_x_env[self.cnt,:,:] = self.tracker_env.y

        self.tracker_env_avg.cycle(self.tracker_pre.y)
        self.psda_x_env_avg[self.cnt,:,:] = self.tracker_env_avg.y

        self.tracker_env_pk.cycle(self.tracker_pre.y)
        self.psda_x_env_pk[self.cnt,:,:] = self.tracker_env_pk.y

        # dynamic amplification

        g_os = np.zeros(self.parseval.y.shape)
        self.pta.cycle(self.tracker_de.y,g_os)
        self.osdpa.cycle(
            self.parseval.y,
            self.tracker_env.y,
            self.tracker_env_avg.y,
            self.tracker_env_pk.y,
            self.tracker_env_vy.y,
            g_os)

        self.bbg_pta[self.cnt,:,:] = self.pta.g
        self.bbg_osdpa[self.cnt,:,:] = self.osdpa.g
        self.bbg_osdpag_post_hi[self.cnt,:,:] = self.osdpa.g_post_hi
        self.bbg_osdpag_post_hi_lt[self.cnt,:,:] = self.osdpa.g_post_hi_lt
        self.bbg_osdpag_prio_hi[self.cnt,:,:] = self.osdpa.g_prio_hi
        self.bbg_osdpa_post_pu_lo[self.cnt,:,:] = self.osdpa.g_post_pu_lo
        self.amp_overshoot[self.cnt,:,:] = self.osdpa.amp_overshoot
        self.psda_x_env_slim[self.cnt,:,:] = self.osdpa.smooth_lim_tracker.y

        G = lscl.dB2amp(np.concatenate(
            (np.zeros(self.pta.g.shape),
            self.pta.g,
            self.osdpa.g),axis=0))

        # gain distribution
        self.distrib.cycle(G)

        # bwd stft
        self.wola_bwd.cycle(X=self.distrib.y*self.wola_fwd.X)
        self.out = self.wola_bwd.x
        
        self.cnt +=1 
