import numpy as np
import apu.apu as apu
import apu.unit.stft.wola as wola

class Unit(apu.Unit):
    def __init__(self, config=dict, inherit_par=dict):
        super().__init__()
        self.cnt = 0
        self.out = np.zeros([inherit_par['onchan'],config['frame_len']])

        # fwd stft
        sub_inherit_par = {
            'sampling_rate': config['sampling_rate'],
            'frame_len': config['frame_len'],
            'nchan': inherit_par['inchan'],
        }
        self.wola_fwd = wola.WolaFwd(
            config['_sub_fwd'],
            inherit_par=sub_inherit_par)
        
        # bwd stft
        sub_inherit_par = {
            'sampling_rate': config['sampling_rate'],
            'frame_len': config['frame_len'],
            'nchan': inherit_par['onchan'],
        }
        self.wola_bwd = wola.WolaBwd(
            config['_sub_bwd'],
            inherit_par=sub_inherit_par)

    def cycle(self,data=np.array) -> None:
        self.wola_fwd.cycle(x=data) 

        self.wola_bwd.cycle(X=self.wola_fwd.X)
        self.out = self.wola_bwd.x

        self.cnt +=1 
