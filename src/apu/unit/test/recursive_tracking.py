import numpy as np
import apu.apu as apu
import apu.unit.stft.wola as wola
import apu.nlt.native.psd as psd
import apu.unit.tracking.recursive as recursive
import apu.unit.bandrouting.coupling as coupling


class Unit(apu.Unit):
    def __init__(self, config=dict, inherit_par=dict):
        super().__init__()

        self.out = np.zeros([inherit_par['onchan'],config['frame_len']])

        sub_inherit_par = {
            'sampling_rate': config['sampling_rate'],
            'frame_len': config['frame_len'],
            'nchan': inherit_par['inchan'],
        }

        self.cnt = 0

        self.wola_fwd = wola.WolaFwd(
            config['_sub_fwd'],
            inherit_par=sub_inherit_par)
        
        # self.wola_bwd = wola.WolaBwd(
        #     config['_sub_bwd'],
        #     inherit_par=sub_inherit_par)

        self.powcoupler = coupling.Coupling(config['_sub_powcoupler'])        
        self.powa_x = np.zeros([inherit_par['nframes'],inherit_par['inchan'],1])

        self.psda_x_lior = np.zeros([inherit_par['nframes'],inherit_par['inchan'],1])
        self.psda_x_lnaiod = np.zeros([inherit_par['nframes'],inherit_par['inchan'],1])
        self.psda_x_lior_ser = np.zeros([inherit_par['nframes'],inherit_par['inchan'],1])
        self.psda_x_lnaiod_ser = np.zeros([inherit_par['nframes'],inherit_par['inchan'],1])
        self.psda_x_lior_par = np.zeros([inherit_par['nframes'],inherit_par['inchan'],1])
        self.psda_x_lnaiod_par = np.zeros([inherit_par['nframes'],inherit_par['inchan'],1])

        sub_inherit_par = {
            'sampling_rate': self.wola_fwd.down_sample_freq,
            'nchan': inherit_par['inchan'],
            'nband': 1,
        }
    
        self.tracker_lior = recursive.Tracker3SegAdaptive(
           config=config['_sub_tracker_lior'],
           inherit_par=sub_inherit_par)

        self.tracker_lnaiod = recursive.Tracker3SegAdaptive(
            config=config['_sub_tracker_lnaiod'],
            inherit_par=sub_inherit_par)
            
        self.tracker_lior_ser = recursive.Tracker3SegPreSmoothAdaptive(
            config=config['_sub_tracker_lior_ser'],
            inherit_par=sub_inherit_par)

        self.tracker_lnaiod_ser = recursive.Tracker3SegPreSmoothAdaptive(
            config=config['_sub_tracker_lnaiod_ser'],
            inherit_par=sub_inherit_par)

        self.tracker_lior_par = recursive.Tracker3SegPreSmoothAdaptive(
            config=config['_sub_tracker_lior_par'],
            inherit_par=sub_inherit_par)

        self.tracker_lnaiod_par = recursive.Tracker3SegPreSmoothAdaptive(
            config=config['_sub_tracker_lnaiod_par'],
            inherit_par=sub_inherit_par)

        # self.psda_X = np.zeros([inherit_par['nframes'],inherit_par['inchan'],self.wola_fwd.M])
        # self.tracker = recursive.Tracker3SegPreSmoothAdaptive(
        #     y=np.zeros([inherit_par['inchan'],self.wola_fwd.M]),
        #     config=config['_sub_tracker'],
        #     inherit_par=sub_inherit_par)

    def cycle(self,data=np.array) -> None:
        self.wola_fwd.cycle(x=data) 

        powa_X = psd.auto(self.wola_fwd.X)  

        self.powcoupler.cycle(powa_X)
        self.powa_x[self.cnt,:,:] = self.powcoupler.y

        # self.tracker.cycle(powa_X)
        # self.psda_X[self.cnt,:,:] = self.tracker.y


        self.tracker_lior.cycle(self.powcoupler.y)
        self.psda_x_lior[self.cnt,:] = self.tracker_lior.y

        self.tracker_lior_ser.cycle(self.powcoupler.y)
        self.psda_x_lior_ser[self.cnt,:] = self.tracker_lior_ser.y

        self.tracker_lior_par.cycle(self.powcoupler.y)
        self.psda_x_lior_par[self.cnt,:] = self.tracker_lior_par.y

        self.tracker_lnaiod.cycle(self.powcoupler.y)
        self.psda_x_lnaiod[self.cnt,:] = self.tracker_lnaiod.y

        self.tracker_lnaiod_ser.cycle(self.powcoupler.y)
        self.psda_x_lnaiod_ser[self.cnt,:] = self.tracker_lnaiod_ser.y

        self.tracker_lnaiod_par.cycle(self.powcoupler.y)
        self.psda_x_lnaiod_par[self.cnt,:] = self.tracker_lnaiod_par.y

        #self.wola_bwd.cycle(X=self.wola_fwd.X)
        #self.out = self.wola_bwd.x

        self.cnt +=1 
