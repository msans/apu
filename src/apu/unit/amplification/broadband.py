#from abc import abstractmethod
import numpy as np
import apu.apu as apu
import apu.nlt.native.timeconst as tc
import apu.nlt.native.logscl as lscl
import apu.unit.tracking.recursive as recursive

class Amp(apu.Unit):
    def __init__(self, config=dict, inherit_par=dict):
        super().__init__()
        self.in_dynrng_calib = config['input_dynrng_calib']
        self.out_dynrng_calib = config['output_dynrng_calib']
        self.delta_calib_rng = self.in_dynrng_calib - self.out_dynrng_calib
        self.sr = inherit_par['sampling_rate']
        self.nchan = inherit_par['nchan']
        self.nband = inherit_par['nband']
        self.g = np.zeros([self.nchan,self.nband])

    # @abstractmethod
    # def cycle(self, frame_in=np.array) -> None:
    #     pass

class PeakTrackingBasedAmp(Amp):
    def __init__(self, config=dict, inherit_par=dict):
        """ Peak Tracking based Amplification Model
        """
        super().__init__(config=config, inherit_par=inherit_par)
        
        self.kcmp = config['kcmp'] - self.delta_calib_rng # out. domain proc virtually shifted in the in. domain
        self.scmp = config['scmp'] 
        self.glim = config['glim'] 
        self.kexp = config['kexp'] - self.in_dynrng_calib # absolute defined (dBSPL)
        self.sexp = config['sexp'] 

    def cycle(self, lvl: np.array, g_os: np.array) -> None:
        """
        Run amplification model
        :param lvl: broadband input level [dB], input level domain scaled
        :param g_os: dynamic gain offset [dB]
        """

        # input domain processing
        g_limit = self.glim + g_os
        g_lo = self.sexp * (lvl - self.kexp) + g_limit  # low-level expansion
        g_pu = g_limit
        g_pu_lo = np.minimum(g_pu,g_lo)

        # i/o recalib (virtual: do nothing, shifted as last step because commutable and less computations required)

        # output domain processing
        g_hi = self.scmp * (lvl - self.kcmp) + self.glim  # hi-level compression
        g = np.minimum(g_hi,g_pu_lo)

        self.g =  g + self.delta_calib_rng  # apply post-poned i/o recalibration
        return None

class Osdp(Amp):
    def __init__(self, config=dict, inherit_par=dict):
        """ Optimal Speech Dynamic Preservation Amplification
        """
        super().__init__(config=config, inherit_par=inherit_par)
        self.es = config['expansion_slope']
        self.ekv = config['valley_expansion_knee'] - self.in_dynrng_calib
        self.eka = self.ekv + config['valley2average_expansion_knee']
        self.skp = config['peak_saturation_knee'] - self.delta_calib_rng   # out. domain proc virtually
                                                                           # shifted in the in. domain
        self.ska = self.skp + config['peak2average_saturation_knee']

        # self.puo = np.minimum(pickup_offset, self.ska - self.eka)
        self.puo = np.minimum(config['pickup_offset'], config['pickup_amp_range'])  # gain: pickup_offset dB at self.eka and
        # 0 dB at self.eka +  pickup_amp_range
        self.ss = config['saturation_slope']

        self.dcb = config['differential_compression_bias']
        
        #self.pus = -self.puo / (self.ska - self.eka)
        self.pus = -self.puo / config['pickup_amp_range']  # FIXME deniminator: the upper bound ska must be replaced by a calibrated point defined indep. of ska and absolutely (i,e, in dBSPL) like the exp. kp

        # long term gain corrrection
        #self.dg_lt_hi = np.zeros([self.nchan,self.nband])
        #self.a = tc.t2a(config['longterm_gain_correction_tc'], self.sr)
        self.dg_lt_hi = recursive.Tracker(
            config={'tc': config['longterm_gain_correction_tc']},
            inherit_par={'nchan': self.nchan,'nband': self.nband, 'sampling_rate': self.sr})

        # smooth limiter
        self.smooth_lim_tracker = recursive.DualExp(
            config={'atk_tc': config['smooth_lim_atk_tc'], 'rel_tc': config['smooth_lim_rel_tc']},
            inherit_par={'nchan': self.nchan,'nband': self.nband, 'sampling_rate': self.sr})
        self.slim = config['smooth_lim'] + self.skp
        # instantaneous limiter
        self.ilim = config['inst_lim'] + self.skp


        # monitorable signal
        self.g_post_pu_lo = np.zeros([self.nchan,self.nband])
        self.g_prio_hi = np.zeros([self.nchan,self.nband])
        self.g_post_hi_lt = np.zeros([self.nchan,self.nband])
        self.g_post_hi = np.zeros([self.nchan,self.nband])

    def cycle(
            self, 
            power: np.array, 
            env: np.array, 
            env_avg: np.array,
            env_pk: np.array, 
            env_vy: np.array, 
            g_os: np.array) -> None:
        """
        Run amplification model
        :param power: instantaneous broadband power
        :param env: envelope level
        :param env_avg: envelope average level
        :param env_pk: envelope peak level
        :param env_vy: envelope valley level
        :param g_os: dynamic gain offset [dB]
        """

        p = lscl.pow2dB(power)
        e = lscl.pow2dB(env)
        e_a = lscl.pow2dB(env_avg)
        e_p = lscl.pow2dB(env_pk)
        e_v = lscl.pow2dB(env_vy)

        puo = self.puo + g_os
        g_prio_ea_lo = self.es * (e_a - self.eka) + puo  # a-priori lo-lvl amp. characteristic
        g_prio_ea_pu = np.minimum(puo,self.pus * (e_a - self.eka) + puo)  # a-priori pick-up amp. characteristic
        g_post_pu_lo = np.minimum(g_prio_ea_pu, g_prio_ea_lo)  # pu/lo-lvl a-priori gain

        g_prio_ea_hi = self.ss * (e_a - self.ska)  # a-priori hi-lvl amp. characteristic
        g_prio_hi = np.minimum(g_prio_ea_hi, g_post_pu_lo)  # hi-lvl a-priori gain

        amp_overshoot = np.maximum(0, e_p + g_prio_hi - self.skp ) # amplification overshoot

        self.dg_lt_hi.cycle(amp_overshoot)
        mod = e - e_a  # modulation

        mod_a = e_p - e_a  # average (upper-end) modulation
        dcs_num = np.maximum(0,amp_overshoot - self.dg_lt_hi.y)
        dcs_den = np.maximum(np.spacing(1), mod_a - self.dcb)
        dcs = np.minimum(1, dcs_num / dcs_den )  # diff. compressor slope in [0,1]
        dg_st_hi = dcs * np.maximum(0, mod)  # hi-lvl short-term corr

        dg_hi = self.dg_lt_hi.y + dg_st_hi  # >= 0

        g_post_hi = g_prio_hi - dg_hi  # gain correction

        # saturation (overshoot := min(0,margin) )
        self.smooth_lim_tracker.cycle(p)
        smooth_margin = self.slim - self.smooth_lim_tracker.y
        inst_margin = self.ilim - p
        g = np.minimum(g_post_hi,smooth_margin,inst_margin) 

        # registry update / output
        #self.dg_lt_hi = dg_lt_hi
        self.g = g + self.delta_calib_rng

        # monitoring
        self.g_post_hi = g_post_hi
        self.g_post_hi_lt = g_prio_hi - self.dg_lt_hi.y
        self.g_prio_hi = g_prio_hi
        self.g_post_pu_lo = g_post_pu_lo
        self.amp_overshoot = amp_overshoot
        return None
