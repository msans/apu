import numpy as np
import apu.apu as apu
import apu.nlt.native.logscl as lscl

class Equalizer(apu.Unit):

    def __init__(self, config=dict, inherit_par=dict):
        super().__init__()
        self.g = np.array(config['gain'])

    def cycle(self, x: np.array) -> None:
        self.y = x*self.g
