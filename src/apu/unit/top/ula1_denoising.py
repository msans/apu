import numpy as np
import scipy.special as spsp
import apu.apu as apu
import apu.unit.stft.wola as wola
import apu.unit.denoising.bf1.static as bf_static
import apu.unit.equalizing.eq as eq
import apu.unit.axpsd.driver as psd_driver
import apu.unit.axpsd.cardioids_worker as psd_worker
import apu.unit.denoising.bf1.gsc_ls_adapt as bf_adapt
import apu.unit.denoising.bf1.gsc as bf_adaptive
import apu.unit.denoising.snm1.lsnm.lsdn as lsdn
import apu.unit.denoising.postsnr.fsbn as fsbn_postsnr
import apu.unit.denoising.postsnr.lsdn as lsdn_postsnr
import apu.unit.denoising.speechprob.apriorisap as apriorisap
import apu.unit.denoising.spectrotemp.meta as st_meta
import apu.unit.denoising.spectrotemp.noreg as st_noreg
import apu.unit.denoising.doamasking.doamask as doamask
import apu.unit.denoising.gfa.fusion as gf
import apu.unit.denoising.gfa.application as ga

class Unit(apu.Unit):
    def __init__(self, config=dict, inherit_par=dict):
        super().__init__()
        self.cnt = 0
        self.out = np.zeros([inherit_par['onchan'],config['frame_len']])

        # fwd stft
        sub_inherit_par = {
            'sampling_rate': config['sampling_rate'],
            'frame_len': config['frame_len'],
            'nchan': inherit_par['inchan'],
        }
        self.wola_fwd = wola.WolaFwd(
            config=config['_sub_fwd'],
            inherit_par=sub_inherit_par)
        
        # static beam-forming
        f = config['sampling_rate'] / self.wola_fwd.N * np.arange(0, self.wola_fwd.M, 1)
        sub_inherit_par = {
            'sampling_rate': config['sampling_rate'],
            'center_frequency': f,
            'bidir': True,

        }
        self.bfs = bf_static.CardioidDual(
            config=config['_sub_bfs'],
            inherit_par=sub_inherit_par)

        self.bfeq = eq.Equalizer(
            config=config['_sub_bfeq'],
            inherit_par={})

        # psd
        nula1 = int(spsp.binom(inherit_par['inchan'],2))
        nbfchan = 2*nula1
        sub_inherit_par = {
            'sampling_rate': self.wola_fwd.down_sample_freq,
            'nband': self.wola_fwd.M,
        }
        self.driver_psd = psd_driver.Driver(
            config=config['_sub_axpsd'],
            inherit_par=sub_inherit_par)
        self.worker_psd = psd_worker.Worker(
            {},
            inherit_par=sub_inherit_par)
        
        sub_inherit_par = {
            'nband': self.wola_fwd.M,
        }
        self.bfcoef = bf_adapt.LsDual(
            config=config['_sub_bfa'],
            inherit_par=sub_inherit_par)

        self.bfa = bf_adaptive.GscDual(
            config={},
            inherit_par=sub_inherit_par)

        self.bfa_psd = bf_adaptive.PsdGscDual(
            config={},
            inherit_par=sub_inherit_par)

        # lsdn source-noise model
        sub_inherit_par = {
            'nband': self.wola_fwd.M,
            'mic_dist': self.bfs.d,
            'sound_speed': self.bfs.c,
            'center_frequency': self.bfs.f,
            'sampling_rate': self.wola_fwd.down_sample_freq,
        }
        self.lsdn = lsdn.LsDnCardioid(
            config=config['_sub_lsdn'],
            inherit_par=sub_inherit_par)

        # lsdn post-snr
        sub_inherit_par = {
            'sampling_rate': self.wola_fwd.down_sample_freq,
            'nchan': nbfchan + 1,
            'nband': self.wola_fwd.M,
        }
        self.lsdn_postsnr = lsdn_postsnr.RebalancedPostSnr(
            config=config['_sub_postsnr_main'],
            inherit_par=sub_inherit_par)

        # fsbn post-snr
        sub_inherit_par = {
            'sampling_rate': self.wola_fwd.down_sample_freq,
            'nband': self.wola_fwd.M,
        }
        self.fsbn_postsnr = fsbn_postsnr.BiasedPostSnrDual(
            config=config['_sub_postsnr_fsbn'],
            inherit_par=sub_inherit_par)

        # apriosap
        sub_inherit_par = {
            'sampling_rate': self.wola_fwd.down_sample_freq,
            'nchan': nbfchan + 1,
            'nband': self.wola_fwd.M,
        }
        self.priosap = apriorisap.NonLinSoftDecisionAdaptiveTracking(
            config=config['_sub_priosap'],
            inherit_par=sub_inherit_par)

        # spectro-temporal denoising
        sub_inherit_par = {
            'sampling_rate': self.wola_fwd.down_sample_freq,
            'nchan': nbfchan + 1,
            'nband': self.wola_fwd.M,
        }
        self.lsdn_st = st_meta.Wrap(
            config=config['_sub_st_main'],
            inherit_par=sub_inherit_par)

        sub_inherit_par = {
            'sampling_rate': self.wola_fwd.down_sample_freq,
            'nchan': nbfchan,
            'nband': self.wola_fwd.M,
        }
        self.fsbn_st = st_noreg.ImlWiener(
            config=config['_sub_st_fsbn'],
            inherit_par=sub_inherit_par)

        # doamask
        sub_inherit_par = {
            'nchan': nbfchan,
            'nband': self.wola_fwd.M,
            'tracked_bands': config['_sub_lsdn']['_sub_lsdoa']['tracked_bands'],
        }
        self.doa_mask = doamask.Masker(
            config=config['_sub_lsdoamask'],
            inherit_par=sub_inherit_par)

        # gain fusion & application
        sub_inherit_par = {
            'nchan': nbfchan,
            'nband': self.wola_fwd.M,
        }
        self.gfusion_bfa = gf.Core(
            config=config['_sub_gf'],
            inherit_par=sub_inherit_par)
        self.gapp_bfa = ga.Core(
            config={},
            inherit_par=sub_inherit_par)
        
        sub_inherit_par = {
            'nchan': 1,
            'nband': self.wola_fwd.M,
        }
        self.gfusion_omni = gf.Core(
            config=config['_sub_gf'],
            inherit_par=sub_inherit_par)
        self.gapp_omni = ga.Core(
            config={},
            inherit_par=sub_inherit_par)

        # bwd stft
        sub_inherit_par = {
            'sampling_rate': config['sampling_rate'],
            'frame_len': config['frame_len'],
            'nchan': inherit_par['onchan'],  # FIXME
        }
        self.wola_bwd = wola.WolaBwd(
            config=config['_sub_bwd'],
            inherit_par=sub_inherit_par)

        self.debug = np.zeros([inherit_par['nframes'],1,self.wola_fwd.M])

    def cycle(self,data=np.array) -> None:
        
        
        # fwd transform
        self.wola_fwd.cycle(x=data)
        m = self.wola_fwd.X[0,:][np.newaxis,:]

        # static beam patterns (cardoids)
        self.bfs.cycle(x=self.wola_fwd.X)

        # psd driver (omni) and workers (cardioids)
        self.driver_psd.cycle(x=self.wola_fwd.X)
        self.worker_psd.cycle(x=self.bfs.y,a=self.driver_psd.a)

        # ls-bf coeff
        self.bfcoef.cycle(axpsd=self.worker_psd.y)

        # adaptive beam patterns
        self.bfa.cycle(c=self.bfs.y,w=self.bfcoef.w)

        # psd adaptive beam patterns
        self.bfa_psd.cycle(psd=self.worker_psd.y,w=self.bfcoef.w)

        # lsdn source-noise model
        self.lsdn.cycle(psd=self.worker_psd.y, w=self.bfcoef.w)
        
        # fsbn post-snr
        self.fsbn_postsnr.cycle(psd_c=self.worker_psd.y[0:2,:])

        # lsdn post-snr
        self.lsdn_postsnr.cycle(psd_x=self.lsdn.psd_s,psd_v=self.lsdn.psd_v)

        # a-priori sap estimation
        self.priosap.cycle(gamma=self.lsdn_postsnr.gamma)

        # spectro-temporal denoising gain
        self.lsdn_st.cycle(
            gamma=self.lsdn_postsnr.gamma, 
            q=self.priosap.priosap,
            psd_x=self.lsdn_postsnr.psd_s+self.lsdn_postsnr.psd_v,
            lambda_d=self.lsdn_postsnr.psd_v)

        # spectro-temporal f/b ratio gain
        self.fsbn_st.cycle(gamma=self.fsbn_postsnr.gamma)

        # doamasking
        self.doa_mask.cycle(self.lsdn.lsdoa.doa)

        # gain fusion & application
        self.gfusion_bfa.cycle(self.lsdn_st.Estimator.g[0:2,:], self.fsbn_st.g, self.doa_mask.g)
        self.gapp_bfa.cycle(x=self.bfa.y, g=self.gfusion_bfa.g)
        self.gfusion_omni.cycle(self.lsdn_st.Estimator.g[2,:])
        self.gapp_omni.cycle(x=m, g=self.gfusion_omni.g)

        # equalizing
        self.bfeq.cycle(self.gapp_bfa.y)
        gapp_bfa_y = self.bfeq.y
        self.bfeq.cycle(self.bfa.y)
        bfa_y = self.bfeq.y
        self.bfeq.cycle(self.bfs.y)
        bfs_y = self.bfeq.y

        # bwd transform
        bwd_in = np.concatenate(
            (gapp_bfa_y,bfa_y,bfs_y,self.gapp_omni.y),
            axis=0)
        self.wola_bwd.cycle(X=bwd_in)

        # output
        self.out = self.wola_bwd.x

        self.debug[self.cnt,0,:] = self.lsdn.lsdoa.doa[np.newaxis,np.newaxis,:]

        self.cnt +=1 
