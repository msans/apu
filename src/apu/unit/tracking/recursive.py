from abc import ABC, abstractmethod
import numpy as np
import apu.apu as apu
import apu.nlt.native.timeconst as tc
import apu.nlt.native.logscl as lscl

class Tracker(apu.Unit):
    def __init__(self, config=dict, inherit_par=dict):
        """
        Tracker
        :param config: config
        :param inherit_par: inherit parameters
        """
        super().__init__()
        self.sr = inherit_par['sampling_rate']
        self.nchan = inherit_par['nchan']
        self.nband = inherit_par['nband']
        t = np.array(config['tc'])
        self.a = np.tile(tc.t2a(t, self.sr),(self.nchan,1))
        self.y = np.zeros([self.nchan,self.nband])

    def _filter(self, x=np.array) -> None:
        self.y = self.a * x + (1 - self.a) * self.y

    def cycle(self, x=np.array) -> None:
        self._filter(x=x)
        return None

class TrackerDriven(Tracker):
    def __init__(self, config=dict, inherit_par=dict):
        """
        Tracker
        :param config: config
        :param inherit_par: inherited parameters
        """
        config['tc'] = 0  # insert dummy /overwrite as pure init
        super().__init__(config=config, inherit_par=inherit_par)

    def cycle(self, x=np.array, a=np.array) -> None:
        self.a = a
        self._filter(x=x)
        return None

class DualExp(apu.Unit):
    def __init__(self, config=dict, inherit_par=dict):
        """
        Tracker
        
        :param config: config
        :param inherit_par: inherited parameters
        """

        self.sr = inherit_par['sampling_rate']
        self.nchan = inherit_par['nchan']
        self.nband = inherit_par['nband']
        self.y = np.zeros([self.nchan,self.nband])

        ta = np.array(config['atk_tc'])
        tr = np.array(config['rel_tc'])
        self.aa = np.tile(tc.t2a(ta, self.sr),(self.nchan,1))
        self.ar = np.tile(tc.t2a(tr, self.sr),(self.nchan,1))

    def cycle(self, x=np.array) -> None:
        s = (x >= self.y).astype(int)
        a = s*self.aa + (1-s)*self.ar
        self.y = a * x + (1 - a) * self.y
        return None

class Tracker3SegAdaptive(Tracker):
    def __init__(self, config=dict, inherit_par=dict):

        c = {'tc': config['atk_tc_fast']}
        super().__init__(config=c, inherit_par=inherit_par)
        
        self.kpmap = {
            'aiod': self._kpmap_passtrough,
            'lior': self._kpmap_passtrough,
            'lnaiod': self._kpmap_lnaiod,
        }
        self.distance = {
            'aiod': self._distance_aiod,
            'lior': self._distance_lior,
            'lnaiod': self._distance_lnaiod,
        }
        
        self.variant = config['variant']

        # map kp
        atk_kp_slow = np.array(config['atk_kp_slow'])
        rel_kp_slow = np.array(config['rel_kp_slow'])
        atk_kp_fast = np.array(config['atk_kp_fast'])
        rel_kp_fast = np.array(config['rel_kp_fast'])
        atk_kp_slow = self.kpmap[self.variant](atk_kp_slow)
        rel_kp_slow = self.kpmap[self.variant](rel_kp_slow)
        atk_kp_fast = self.kpmap[self.variant](atk_kp_fast)
        rel_kp_fast = self.kpmap[self.variant](rel_kp_fast)

        # read kp
        
        self.k_as = np.tile(atk_kp_slow,(self.nchan,1))
        self.k_rs = np.tile(rel_kp_slow,(self.nchan,1))
        self.k_af = np.tile(atk_kp_fast,(self.nchan,1))
        self.k_rf = np.tile(rel_kp_fast,(self.nchan,1))

        # convert time const to coeff
        atk_tc_slow = np.array(config['atk_tc_slow'])
        rel_tc_slow = np.array(config['rel_tc_slow'])
        atk_tc_fast = np.array(config['atk_tc_fast'])
        rel_tc_fast = np.array(config['rel_tc_fast'])
        self.a_as = np.tile(tc.t2a(atk_tc_slow, self.sr),(self.nchan,1))
        self.a_af = np.tile(tc.t2a(atk_tc_fast, self.sr),(self.nchan,1))
        self.a_rs = np.tile(tc.t2a(rel_tc_slow, self.sr),(self.nchan,1))
        self.a_rf = np.tile(tc.t2a(rel_tc_fast, self.sr),(self.nchan,1))

        # compute mapping slope
        self.s_a = (self.a_af - self.a_as) / (self.k_af - self.k_as)
        self.s_r = (self.a_rf - self.a_rs) / (self.k_rf - self.k_rs)

        self.d = np.zeros(self.y.shape)
        self.s = np.full(self.y.shape, True)

    def _kpmap_passtrough(self, kp: np.array) -> np.array:
        return kp

    def _kpmap_lnaiod(self, kp: np.array) -> np.array:
        kplin = lscl.dB2pow(kp)
        return lscl.pow2dB((kplin-1)/(kplin+1))

    def _distance_aiod(self, x: np.array) -> None:
        self.s = (x >= self.y).astype(int)
        self.d = abs(x - self.y)
        return None

    def _distance_lior(self, x: np.array) -> None:
        self.s = (x >= self.y).astype(int)
        self.d = abs(lscl.pow2dB(x) - lscl.pow2dB(self.y))
        return None

    def _distance_lnaiod(self, x: np.array) -> None:
        self.s = (x >= self.y).astype(int)
        self.d = lscl.pow2dB(np.abs(x-self.y)) - lscl.pow2dB(x+self.y)
        return None
        
# self.d = abs(lscl.pow2dB(np.abs(x-self.y)) - 0.5*lscl.pow2dB(x*self.y))
# self.d = abs(lscl.pow2dB(np.abs(x*self.y)) - lscl.pow2dB(x**2+self.y**2))

    def _mapping(self) -> None:
        nots = 1-self.s
        a_f = self.s * self.a_af + nots * self.a_rf
        a_s = self.s * self.a_as + nots * self.a_rs
        k_s = self.s * self.k_as + nots * self.k_rs
        s = self.s * self.s_a + nots * self.s_r
    
        self.a = np.minimum(np.maximum((self.d - k_s)*s+a_s,a_s),a_f)

    def _update(self, x=np.array) -> None:
        self.distance[self.variant](x=x)
        self._mapping()

    def cycle(self, x=np.array) -> None:
        self._update(x=x)
        self._filter(x=x)
        return None

class Tracker3SegPreSmoothAdaptive(Tracker3SegAdaptive):
    def __init__(self, config=dict, inherit_par=dict):
        """
        Tracker
        :param config: config
        :param inherit_par: inherited parameters
        """
        super().__init__(config=config, inherit_par=inherit_par)

        self.route = {
            'serial': self._route_serial,
            'parallel': self._route_parallel,
        }

        self.x = np.zeros(self.y.shape)
        self.z = np.zeros(self.y.shape)
        
        atk_tc_pre = np.array(config['atk_tc_pre'])
        rel_tc_pre = np.array(config['rel_tc_pre'])
        a_ap = tc.t2a(atk_tc_pre,self.sr)
        a_rp = tc.t2a(rel_tc_pre,self.sr)
        if config['pre_disable']:
            a_ap = np.ones(a_ap.shape)
            a_rp = np.ones(a_rp.shape)    
        self.a_ap = np.tile(a_ap,(self.nchan,1))
        self.a_rp = np.tile(a_rp,(self.nchan,1))        
        self.variant_pre = config['variant_pre']

    def _presmooth(self, x=np.array) -> None:
        s = (x >= self.z).astype(int)
        self.a_z = s * self.a_ap + (1-s) * self.a_rp
        self.z = self.a_z * x + (1 - self.a_z) * self.z

    def _route_serial(self, x=np.array):
        self.x = self.z

    def _route_parallel(self, x=np.array):
        self.x = x

    def cycle(self, x=np.array) -> None:
        self._presmooth(x=x)
        self.route[self.variant_pre](x=x)
        self._update(x=self.z)
        self._filter(x=self.x)
        return None
