import numpy as np
import apu.apu as apu
import apu.nlt.native.psd as psd

class Coupling(apu.Unit):
    def __init__(self, config=dict):
        super().__init__()
        self.iomap = np.array(config['iomap'])
        self.nband = self.iomap.shape[0]
        self.nfactor = np.ones(self.nband)
        if config['norm']:
            for k in range(self.nband):
                self.nfactor[k] = self.iomap[k,1] - self.iomap[k,0]

        self.y = None

    def cycle(self, x: np.array) -> np.array:
        self.y = np.zeros([x.shape[0],self.nband], dtype=x.dtype)
        for k in range(self.nband):
            self.y[:,k] = np.sum(
                x[:,self.iomap[k,0]:self.iomap[k,1]],
                axis=-1) / self.nfactor[k]

class Parseval(apu.Unit):
    def __init__(self, inherit_par=dict):
        super().__init__()

        self.scaling = 1/inherit_par['awin_energy']
        if not(inherit_par['norm_sspec']):
            self.scaling = 2*self.scaling
        
        powcoupler_config = {
            'iomap': np.array([[0,inherit_par['nband']]]),
            'norm': True}
        self.powcoupler = Coupling(powcoupler_config)

    def cycle(self, X=np.array) -> np.array:


        # Parseval for DFT with 
        # - no norm at analysis stage
        # - full normalization at synthesis sstgae
        # formula: sum(x**2) = 1/N*sum(X*conjugate(X))
        # implemented by: psd + powercoupling with scaling 1/N
        # condition: 
        # compensation for normlization applied at anaylsis stage
        # 1) half-spectrum energy: assumed done at fwd dft stage
        # 2) window energy 
        # 3) normalization (forward/ortho/backward)

        psd_X = psd.auto(X)
        self.powcoupler.cycle(psd_X)
        self.y = self.powcoupler.y * self.scaling
