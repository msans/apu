import numpy as np
import apu.apu as apu

class Distribution(apu.Unit):
    def __init__(self, config=dict):
        super().__init__()
        self.iomap = np.array(config['iomap'])
        self.nband = self.iomap.size
        self.y = None

    def cycle(self, x: np.array) -> None:
        self.y = x[:,self.iomap]
        return None
