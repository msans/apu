from abc import abstractmethod
import numpy as np
import apu.apu as apu
import apu.unit.tracking.recursive as recursive
import apu.nlt.native.psd as psd

class Worker(apu.Unit):
    def __init__(self, config=dict, inherit_par=dict):
        super().__init__()
        self.nband = inherit_par['nband']

        sub_inherit_par = {
            'sampling_rate': inherit_par['sampling_rate'],
            'nchan': 3,
            'nband': inherit_par['nband'],
        }
        self.tracker = recursive.TrackerDriven(
            config={},
            inherit_par=sub_inherit_par)

        self.y = np.zeros([1,self.nband])
    
    def cycle(self, x=np.array, a=np.array) -> None:
        psd_x = np.concatenate(
            (psd.auto(x),(np.real(psd.cross_dualchan(x)))[np.newaxis,:]),
            axis=0)
        self.tracker.cycle(x=psd_x, a=a)
        self.y = self.tracker.y

        
