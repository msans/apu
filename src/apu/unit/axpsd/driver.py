from abc import abstractmethod
import numpy as np
import apu.apu as apu
import apu.unit.tracking.recursive as recursive
import apu.nlt.native.psd as psd

class Driver(apu.Unit):
    def __init__(self, config=dict, inherit_par=dict):
        super().__init__()
        self.nband = inherit_par['nband']

        sub_inherit_par = {
            'sampling_rate': inherit_par['sampling_rate'],
            'nchan': 1,
            'nband': inherit_par['nband'],
        }
        self.tracker = recursive.Tracker3SegPreSmoothAdaptive(
            config=config['_sub_driver'],
            inherit_par=sub_inherit_par)

        self.y = np.zeros([1,self.nband])
        self.alpha = np.zeros([1,self.nband])

    def cycle(self, x=np.array) -> None:
        psd_sup_x = psd.auto(np.max(x,axis=0,keepdims=True))
        self.tracker.cycle(psd_sup_x)
        self.y = self.tracker.y
        self.a = self.tracker.a

        
