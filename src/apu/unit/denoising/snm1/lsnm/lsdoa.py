import numpy as np
import apu.apu as apu

class LsDoaTracker(apu.Unit):
    """Localizes source direction of arrival tracker"""
    
    def __init__(self, config=dict, inherit_par=dict):
        super().__init__()

        self.nband = inherit_par['nband']
        self.d = inherit_par['mic_dist']
        self.c = inherit_par['sound_speed']
        f = inherit_par['center_frequency']

        self.kd = np.minimum(np.pi, 2 * np.pi * f / self.c * self.d)
        self.rkd = 1/np.maximum(np.spacing(1), self.kd)

        self.idx = range(config['tracked_bands'][0],config['tracked_bands'][1])

        self.dphs = np.zeros(self.nband)
        self.doa = np.zeros(self.nband)

    def _dphs_2_doa(self) -> None:
        """Convert phase shift to doa
        :param dphs: phase shift
        :param rkd: 1/k/d
        """
        cos_doa = self.dphs[1:] * self.rkd[1:]
        self.doa[1:] = np.arccos(np.maximum(-1, np.minimum(1, cos_doa)))
        return None

    def cycle(self, dphs=np.array, alpha=np.array) -> None:

        a = alpha[self.idx]
        self.dphs[self.idx] = a * dphs[self.idx] + (1 - a) * self.dphs[self.idx]
        self._dphs_2_doa()
        return None
