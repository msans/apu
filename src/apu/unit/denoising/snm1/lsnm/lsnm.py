import numpy as np
from abc import abstractmethod
import apu.apu as apu
import apu.unit.denoising.snm1.lsnm.lsdoa as lsdoa
import apu.nlt.native.conditionalspp as cspp
import apu.nlt.native.timeconst as tc

class LocalizedSourceNoiseModel(apu.Unit):
    def __init__(self, config=dict, inherit_par=dict):
        super().__init__()        
        
        self.nchan = 3
        self.nband = inherit_par['nband']
        self.d = inherit_par['mic_dist']
        self.c = inherit_par['sound_speed']
        f = inherit_par['center_frequency']

        # kd:  must be kept in [0,pi], otherwise, spatial aliasing
        self.kd = np.minimum(np.pi, 2 * np.pi * f / self.c * self.d)
        self.rkd = 1/np.maximum(np.spacing(1), self.kd)
        self.f = self.kd / self.d * self.c / np.pi / 2  # saturated f
        self.cos_kd = np.cos(self.kd)  # in [-1,1]
        self.sin_kd = np.sin(self.kd) # in [0,1]

        self.psd_v = np.zeros([self.nchan, self.nband])
        self.psd_s = np.zeros([self.nchan, self.nband])
        self.dphs = np.ones(self.nband)*np.pi/2


        # dphs tracking
        self.priospp = config['priospp']
        a_dphs = tc.t2a(np.array(config['tc_dphs']),inherit_par['sampling_rate'])
        self.a_dphs_inf = a_dphs[1]
        self.da_dphs = a_dphs[0] - a_dphs[1]

        # ls doa
        self.lsdoa = lsdoa.LsDoaTracker(
            config=config,
            inherit_par=inherit_par)

    def _lsd_factor(self, w=np.array) -> np.array:
        """Get the localized source distortion factor from non-steered ULA 1 BF
        :param w: BM matrix factor
        """
        dphs = np.concatenate(
            (self.lsdoa.dphs[np.newaxis,:], -self.lsdoa.dphs[np.newaxis,:]),
            axis=0)
        return (1 - np.cos(self.kd + dphs)) - 2*np.real(w)*(np.cos(dphs) - self.cos_kd) \
            + (np.abs(w)**2) * (1 - np.cos(self.kd - dphs))

    def _cspp(self, q=np.array) -> np.array:
        
        ksi = self.psd_s[-1,:] / np.maximum(np.spacing(1),self.psd_v[-1,:])
        gamma = ksi + 1
        p, _, _ = cspp.gaussian_model(gamma=gamma, ksi=ksi, q=q)  # p, nu, gw
        return p
        
    @abstractmethod
    def cycle(self, psd=np.array, w=np.array) -> None:
        pass
