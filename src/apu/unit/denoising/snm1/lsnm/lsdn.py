import numpy as np
from abc import abstractmethod
import apu.unit.denoising.snm1.lsnm.lsnm as lsnm
import apu.nlt.native.phasor as phasor
import apu.nlt.native.logscl as lscl

class LocalizedSourcePlusDiffuseNoise(lsnm.LocalizedSourceNoiseModel):
    """"Localized source plus diffuse noise model"""

    def __init__(self, config=dict, inherit_par=dict):
    
        super().__init__(config=config['_sub_lsdoa'], inherit_par=inherit_par)

        self.w_cnf = np.minimum(1, np.maximum(0, config['_sub_psd']['cnf_weight']))
        
        self.appw = config['_sub_psd']['apply_bf_coeff']
        
        if self.appw in ['adaptive']:
            pass
        elif self.appw in ['fixed','fixed_difN']:
            w = phasor.delaying_phasor(
            self.d,
            0,
            self.c,
            self.f)
            self.w_cardioid = np.concatenate((
                w[np.newaxis,:],
                w[np.newaxis,:]),axis=0)
        else:
            self.appw = "off"

        # cross-coherence noise field
        self.sinc_kd = np.maximum(0,np.sinc(2 * self.f / self.c * self.d))
        # np.sinc equivalent to: np.minimum(1, m + np.sin(kd) / (kd + m * np.spacing(1)))
        #   with kd = 2*np.pi*f/c*d and m = (f == 0).astype('float')
        #   with sinc(x) = sin(pi*x)/(pi*x) normalize x = 2*pi*f/c*d by pi x/pi = 2*f/c*d
        # saturate >= 0, because sinc(kd) < 0 if spatial aliased bin, i.e. if f > c/d/2

        self.cross_gamma = np.minimum(1,self.sinc_kd * self.w_cnf)  # in [0,1]
        self.kappa = np.maximum(np.spacing(1), 1 - self.cross_gamma * self.cos_kd)  # in ]0,2]
        self.eta = self.cross_gamma - self.cos_kd

    def _dnc_factor(self, w=np.array) -> np.array:
        """Get the diffuse noise canceler factor for non-steered ULA 1 BF
        :param w: BM matrix factor
        """
        return ((1 + np.abs(w)**2)*self.kappa - 2*np.real(w)*self.eta)/2

    @abstractmethod
    def cycle(self, psd=np.array, w=np.array) -> None:
        pass

class LsDnCardioid(LocalizedSourcePlusDiffuseNoise):
    """Localizes source plus diffuse noise field based on cardioid patterns"""
    
    def __init__(self, config=dict, inherit_par=dict):

        super().__init__(config=config, inherit_par=inherit_par)

        self.upsilon = np.maximum(0, 1 - self.cross_gamma ** 2)  # in ]0,1]
        self.rupsilon = 1 / np.maximum(np.spacing(1), self.upsilon)  # in ]0,inf
        self.sin2_kd = self.sin_kd**2  # in ]0,1]
        self.rsin2_kd = 1 / np.maximum(np.spacing(1), self.sin2_kd)  # in ]0,inf

    def cycle(self, psd=np.array, w=np.array) -> None:

        # psd

        psd_sum = psd[0,:] + psd[1,:]
        psd_dif = psd[0,:] - psd[1,:]
        beta = 2*psd[2,:] * self.kappa - psd_sum * self.eta
        delta = beta ** 2 + psd_dif ** 2 * self.sin2_kd * self.upsilon # discriminant of the quadratic polynomial
  
        psd_s = np.maximum(np.spacing(1),
                                    (beta * self.cross_gamma + np.sqrt(delta)) * self.rsin2_kd * self.rupsilon)
        psd_v = np.maximum(np.spacing(1),
                                    (psd_sum + self.cos_kd * 2 * psd[2,]) * self.rsin2_kd - psd_s)
        

        tan_dphs_num = psd_dif * self.cos_kd
        tan_dphs_den = (psd_v * self.kappa - psd_sum + psd_s) * self.sin_kd

        m = (tan_dphs_den == 0).astype('float')
        tan_dphs_den = m * np.spacing(1) + (1-m) * tan_dphs_den
        self.dphs = np.arctan2(tan_dphs_num, tan_dphs_den)

        # omni
        self.psd_v = np.concatenate((
            psd_v[np.newaxis,:],
            psd_v[np.newaxis,:],
            psd_v[np.newaxis,:]),axis=0)
        self.psd_s = np.concatenate((
            psd_s[np.newaxis,:],
            psd_s[np.newaxis,:],
            psd_s[np.newaxis,:]),axis=0)
        if self.appw == 'adaptive':
            # beamformed diffuse noise psd 
            self.psd_v[0:2,:] = self.psd_v[0:2,:] * self._dnc_factor(w=w)
            # beamformed localized source psd
            self.psd_s[0:2,:] = self.psd_s[0:2,:] * self._lsd_factor(w=w)
        elif self.appw == 'fixed':
            # beamformed diffuse noise psd 
            self.psd_v[0:2,:] = self.psd_v[0:2,:] * self._dnc_factor(w=self.w_cardioid)
            # beamformed localized source psd
            self.psd_s[0:2,:] = self.psd_s[0:2,:] * self._lsd_factor(w=self.w_cardioid)
        elif self.appw == 'fixed_difN':
            # beamformed diffuse noise psd 
            self.psd_v[0:2,:] = self.psd_v[0:2,:] * self._dnc_factor(w=self.w_cardioid)        
        else:
            pass
            
        # lsdoa
        #ldr = lscl.pow2dB(psd_s) - lscl.pow2dB(psd_v)
        #a_alt = np.minimum(self.dk,np.maximum(0,ldr-self.k))*self.s
        priosap = 1-self.priospp
        p = self._cspp(q=priosap)
        a = np.minimum(1,np.maximum(0,(p - self.priospp)/priosap))*self.da_dphs + self.a_dphs_inf
        self.lsdoa.cycle(dphs=self.dphs, alpha=a)

        return None
