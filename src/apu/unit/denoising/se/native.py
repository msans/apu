import numpy as np
import apu.apu as apu
import apu.nlt.native.logscl as lscl
import apu.nlt.native.conditionalspp as cspp
import apu.nlt.native.se as se
import apu.nlt.approx.se as se_approx

class SpeechEstimator(apu.Unit):
    def __init__(self, config=dict, inherit_par=dict):
        super().__init__()

        self.sr = inherit_par['sampling_rate']
        self.nchan = inherit_par['nchan']
        self.nband = inherit_par['nband']

        self.g_inf = lscl.dB2amp(config['g_inf'])
        self.g = np.ones([self.nchan, self.nband])

class MagnSpecSub(SpeechEstimator):
    """Speech estimator magnitude spectral subtraction approach
    [1]:
        BOLL, Steven.
        Suppression of acoustic noise in speech using spectral subtraction.
        IEEE Transactions on acoustics, speech, and signal processing, 1979, vol. 27, no 2, p. 113-120.
    """

    def __init__(self, config=dict, inherit_par=dict):
        super().__init__(config=config, inherit_par=inherit_par)

        self.g = np.ones([self.nchan, self.nband])

    def cycle(self, psd_x=np.array, lambda_d=np.array) -> None:
        self.g = np.maximum(
            self.g_inf, 
            se.magnspecsub(psd_x=psd_x, lambda_d=lambda_d))

class Wiener(SpeechEstimator):
    """Speech estimator using Wiener approach.
    [1]:
        WIENER, Norbert.
        Extrapolation, interpolation, and smoothing of stationary time series: with engineering applications.
        The MIT press, 1949.
    """

    def __init__(self, config=dict, inherit_par=dict):
        super().__init__(config=config, inherit_par=inherit_par)

        self.g = np.ones([self.nchan, self.nband])

    def cycle(self, ksi=np.array) -> None:
        self.g = np.maximum(self.g_inf, se.wiener(ksi=ksi))

class PowSpecSub(SpeechEstimator):
    """Speech estimator power spectral subtraction approach
    [1]:
        LIM, Jae Soo et OPPENHEIM, Alan V.
        Enhancement and bandwidth compression of noisy speech.
        Proceedings of the IEEE, 1979, vol. 67, no 12, p. 1586-1604.
    """

    def __init__(self, config=dict, inherit_par=dict):
        super().__init__(config=config, inherit_par=inherit_par)

        self.g = np.ones([self.nchan, self.nband])

    def cycle(self, ksi=np.array) -> None:
        self.g = np.maximum(self.g_inf, se.powspecsub(ksi=ksi))

class OmWiener(SpeechEstimator):
    """Speech estimator using Wiener approach., with optimal modif.
    """

    def __init__(self, config=dict, inherit_par=dict):
        super().__init__(config=config, inherit_par=inherit_par)

        self.gamma_sup = lscl.dB2pow(30)
        self.g_h0 = self.g_inf

        self.g = np.ones([self.nchan, self.nband])

    def cycle(self, gamma=np.array, ksi=np.array, q=np.array) -> None:
        g_h1 = np.maximum(self.g_inf, se.wiener(ksi=ksi))
        p, nu, _ = cspp.gaussian_model(gamma=gamma,ksi=ksi,q=q)
        self.g = np.maximum(self.g_h0, g_h1) * p + self.g_h0 * (1 - p)

class OmStSa(SpeechEstimator):
    """Speech estimator using the decision directed approach, with optimal modif.
    [1]:
        EPHRAIM, Yariv et MALAH, David.
        Speech enhancement using a minimum-mean square error short-time spectral amplitude estimator.
        IEEE Transactions on acoustics, speech, and signal processing, se_approx
        Optimal speech enhancement under signal presence uncertainty using log-spectral amplitude estimator.
        IEEE Signal processing letters, 2002, vol. 9, no 4, p. 113-116.
    """

    def __init__(self, config=dict, inherit_par=dict):
        super().__init__(config=config, inherit_par=inherit_par)

        self.gamma_sup = lscl.dB2pow(30)
        self.g_h0 = self.g_inf

        self.g = np.ones([self.nchan, self.nband])

    def cycle(self, gamma=np.array, ksi=np.array, q=np.array) -> None:
        gamma = np.minimum(gamma, self.gamma_sup)
        p, nu, _ = cspp.gaussian_model(gamma=gamma,ksi=ksi,q=q)
        g_h1 = se.sa(gamma=gamma, nu=nu)
        self.g = np.maximum(self.g_h0, g_h1) * p + self.g_h0 * (1 - p)


class OmStLsa(SpeechEstimator):
    """Speech estimator using the decision directed approach
    [1]:
        EPHRAIM, Yariv et MALAH, David.
        Speech enhancement using a minimum mean-square error log-spectral amplitude estimator.
        IEEE transactions on acoustics, speech, and signal processing, 1985, vol. 33, no 2, p. 443-445.
    [2]:
        COHEN, Israel. 
        Optimal speech enhancement under signal presence uncertainty using log-spectral amplitude estimator.
        IEEE Signal processing letters, 2002, vol. 9, no 4, p. 113-116.
    """

    def __init__(self, config=dict, inherit_par=dict):
        super().__init__(config=config, inherit_par=inherit_par)

        self.gamma_sup = lscl.dB2pow(30)
        self.g_h0 = self.g_inf
        
        if config['approx_enable']:
            self.se = se_approx.lsa
        else:
            self.se = se.lsa

        self.g = np.ones([self.nchan, self.nband])

    def cycle(self, gamma=np.array, ksi=np.array, q=np.array) -> None:
        gamma = np.minimum(gamma, self.gamma_sup)
        p, nu, gw = cspp.gaussian_model(gamma=gamma,ksi=ksi,q=q)
        g_h1 = self.se(nu=nu, gw=gw)
        self.g = (np.maximum(self.g_h0, g_h1) ** p) * (self.g_h0 ** (1 - p))

class WarpedWiener(SpeechEstimator):
    """Speech estimator using Wiener approach., with optimal modif.
    """

    def __init__(self, config=dict, inherit_par=dict):
        super().__init__(config=config, inherit_par=inherit_par)

        self.gamma_sup = lscl.dB2pow(30)
        self.g_h0 = self.g_inf
        self.k = config['k']
        self.x0 = config['x0']
        self.ksi_g = lscl.dB2pow(config['ksi_bias'])
        self.gh1_g = lscl.dB2amp(config['gh1_bias'])
        self.g = np.ones([self.nchan, self.nband])

    def cycle(self, gamma=np.array, ksi=np.array, q=np.array) -> None:
        g_h1 = np.minimum(1,np.maximum(self.g_inf, self.gh1_g * se.wiener(ksi=(ksi*self.ksi_g))))
        p, nu, _ = cspp.warped_gaussian_model(
            gamma=gamma,
            ksi=ksi,
            q=q,
            k=self.k,
            x0=self.x0)
        self.g = np.maximum(self.g_h0, g_h1) * p + self.g_h0 * (1 - p)
