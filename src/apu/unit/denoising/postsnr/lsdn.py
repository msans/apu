import numpy as np
import apu.apu as apu
import apu.nlt.native.logscl as lscl
import apu.nlt.native.timeconst as tc
import apu.unit.bandrouting.coupling as coupling
import apu.unit.bandrouting.distribution as distribution
import apu.unit.denoising.nve.unbiasedmmse as unbiasedmmse
import apu.unit.tracking.recursive as recursive

class RebalancedPostSnr(apu.Unit):
    def __init__(self, config=dict, inherit_par=dict):
        super().__init__()

        self.sr = inherit_par['sampling_rate']
        self.nchan = inherit_par['nchan']
        self.nband = inherit_par['nband']

        # localized noise
        self.nve_cpl = config['_sub_lsdn']['lnve_coupling']
        if self.nve_cpl:
            self.coupler = coupling.Coupling(config=config['_sub_coupling'])
            self.distributor = distribution.Distribution(config=config['_sub_distribution'])
            sub_inherit_par = {
            'sampling_rate': inherit_par['sampling_rate'],
            'nchan': inherit_par['nchan'],
            'nband': self.coupler.nband,
            }
        else:
            sub_inherit_par = inherit_par
        
        self.ln_subtraction = config['_sub_lsdn']['ln_subtraction']
        self.nve = unbiasedmmse.UnBiasedMmse(
            config=config['_sub_nve'],
            inherit_par=sub_inherit_par)  

        ln_weight_o = lscl.dB2pow(np.array(config['_sub_lsdn']['ln_weight_omni']))
        ln_weight_bf = lscl.dB2pow(np.array(config['_sub_lsdn']['ln_weight_bf']))
        self.ln_penalty = np.minimum(1,lscl.dB2pow(config['_sub_lsdn']['ln_penalty']))
        self.ln_weight = np.concatenate((
                ln_weight_bf[np.newaxis,:],
                ln_weight_bf[np.newaxis,:],
                ln_weight_o[np.newaxis,:]),axis=0)*self.ln_penalty

        dn_weight_o = lscl.dB2pow(np.array(config['_sub_lsdn']['dn_weight_omni']))
        dn_weight_bf = lscl.dB2pow(np.array(config['_sub_lsdn']['dn_weight_bf']))
        self.dn_penalty = np.minimum(1,lscl.dB2pow(config['_sub_lsdn']['dn_penalty']))
        self.dn_weight = np.concatenate((
                dn_weight_bf[np.newaxis,:],
                dn_weight_bf[np.newaxis,:],
                dn_weight_o[np.newaxis,:]),axis=0)*self.dn_penalty

        self.psd_s = np.zeros([self.nchan,self.nband])
        self.psd_v = np.zeros([self.nchan,self.nband])
        self.psd_d = np.zeros([self.nchan,self.nband])
        self.gamma = np.zeros([self.nchan,self.nband])
        self.ksi_ml = np.zeros([self.nchan,self.nband])

    def cycle(self, psd_x: np.array, psd_v: np.array) -> None:

        self.psd_v = np.maximum(np.spacing(1),psd_v)*self.dn_weight

        if self.ln_subtraction:
            # sub-band coupling
            if self.nve_cpl:
                self.coupler.cycle(x=psd_x)
                psd_x_sbc = self.coupler.y
                self.coupler.cycle(x=self.psd_d)
                psd_d_sbc = self.coupler.y
            else:
                psd_x_sbc = psd_x
                psd_d_sbc = self.psd_d

            # localized noise from localized source subtraction
            self.nve.cycle(psd_y=psd_x_sbc) # noise variance estimation
            if self.nve_cpl:
                self.distributor.cycle(x=self.nve.psd_d)
                #self.distributor.cycle(x=self.nve.lambda_d)
                self.psd_d = self.distributor.y * self.ln_weight
            else:
                self.psd_d = self.nve.psd_d * self.ln_weight
                #self.psd_d = self.nve.lambda_d * self.ln_weight
        
        self.psd_v = self.psd_v + self.psd_d 
        self.psd_s = psd_x - self.psd_d
            
        self.gamma = np.maximum(1+np.spacing(1), psd_x / self.psd_v)
        self.ksi_ml = self.gamma - 1
