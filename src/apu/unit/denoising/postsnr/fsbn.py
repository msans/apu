import numpy as np
import apu.apu as apu
import apu.nlt.native.logscl as lscl

class BiasedPostSnr(apu.Unit):
    def __init__(self, config=dict, inherit_par=dict):
        super().__init__()

        self.nband = inherit_par['nband']
        self.nchan = inherit_par['nchan']
        self.bias = lscl.dB2pow(config['bias'])

        self.ksi = np.zeros([self.nchan,self.nband])
        self.gamma = np.zeros([self.nchan,self.nband])

class BiasedPostSnrSingle(BiasedPostSnr):
    def __init__(self, config=dict, inherit_par=dict):
        inherit_par['nchan'] = 1
        super().__init__(config=config, inherit_par=inherit_par)

    def cycle(self, psd_c: np.array) -> None:
        psd_d = np.maximum(np.spacing(1),psc_c[1,:]*self.bias)
        psd_s = psc_c[0,:]
        self.ksi =  psd_s / psd_d
        self.gamma = (psd_s + psd_d) / psd_d

class BiasedPostSnrDual(BiasedPostSnr):
    def __init__(self, config=dict, inherit_par=dict):
        inherit_par['nchan'] = 2
        super().__init__(config=config, inherit_par=inherit_par)

    def cycle(self, psd_c=np.array) -> None:
        psd_d = np.concatenate((
            np.maximum(np.spacing(1),psd_c[1,:][np.newaxis,:]*self.bias),
            np.maximum(np.spacing(1),psd_c[0,:][np.newaxis,:]*self.bias)),axis=0)
        psd_s = psd_c
        self.ksi =  psd_s / psd_d
        self.gamma =  (psd_s + psd_d) / psd_d
        
