#from abc import abstractmethod
import numpy as np
import apu.apu as apu


class Gsc(apu.Unit):
    def __init__(self, config=dict, inherit_par=dict):
        super().__init__()
        self.nband = inherit_par['nband']        
        # FIXME: add par for round-robin update

    # @abstractmethod
    # def cycle(self) -> None:
    #     pass

class GscSingle(Gsc):
    def __init__(self, config=dict, inherit_par=dict):
        super().__init__(config=config, inherit_par=inherit_par)
        self.y = np.zeros([1,self.nband])

    def cycle(self, t=np.array, b=np.array, w=np.array) -> None:
        self.y = t - w * b

class GscDual(Gsc):
    def __init__(self, config=dict, inherit_par=dict):
        super().__init__(config=config, inherit_par=inherit_par)
        self.y = np.zeros([2,self.nband])

    def cycle(self, c=np.array, w=np.array) -> None:
        self.y = c - w * np.flipud(c)

class PsdGscDual(Gsc):
    def __init__(self, config=dict, inherit_par=dict):
        super().__init__(config=config, inherit_par=inherit_par)
        self.y = np.zeros([3,self.nband])

    def cycle(self, psd=np.array, w=np.array) -> None:

        # apsd beam 0
        self.y[0,:] = psd[0,:] - 2 * w[0,:] * psd[2,:] + (w[0,:] ** 2) * psd[1,:]
        # apsd beam 1
        self.y[1,:] = psd[1,:] - 2 * w[1,:] * psd[2,:] + (w[1,:] ** 2) * psd[0,:]
        # xpsd beam0,1
        self.y[2,:] = (1 + w[0, :] * w[1, :]) * psd[2, :] - w[1, :] * psd[0, :] - w[0, :] * psd[1, :]
        
        
       
