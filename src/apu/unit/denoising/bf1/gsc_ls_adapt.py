#from abc import abstractmethod
import numpy as np
import apu.apu as apu


class Ls(apu.Unit):
    def __init__(self, config=dict, inherit_par=dict):
        super().__init__()
        self.kappa = config['kappa']
        self.nband = inherit_par['nband']
        # FIXME: add par for round-robin update

    def ls_coeff(self,bt=np.array,bb=np.array) ->None:
        self.w = np.minimum(
            1,
            np.maximum(
                0,
                bt/np.maximum(np.spacing(1),bb+self.kappa)))

    # @abstractmethod
    # def cycle(self) -> None:
    #     pass


class LsSingle(Ls):
    def __init__(self, config=dict, inherit_par=dict):
        super().__init__(config=config, inherit_par=inherit_par)
        self.w = np.zeros([1,self.nband])
        self.y = np.zeros([1,self.nband])

    def cycle(self, apsd=np.array, xpsd=np.array) -> None:
        self.ls_coeff(bt=xpsd, bb=apsd)

class LsDual(Ls):
    def __init__(self, config=dict, inherit_par=dict):
        super().__init__(config=config, inherit_par=inherit_par)
        self.w = np.zeros([2,self.nband])
        self.y = np.zeros([2,self.nband])

    def cycle(self, axpsd=np.array) -> None:
        bb = np.flipud(axpsd[0:2,:])
        bt = axpsd[np.newaxis,2,:]
        bt = np.concatenate((bt, bt),axis=0)
        self.ls_coeff(bt=bt, bb=bb)
