import numpy as np
import apu.apu as apu
import apu.nlt.native.phasor as phasor

class Cardioid(apu.Unit):
    def __init__(self, config=dict, inherit_par=dict):
        super().__init__()
        self.d = config['mic_dist']
        self.c = config['sound_speed']
        self.f = inherit_par['center_frequency']

        w = phasor.delaying_phasor(
            self.d,
            0,
            self.c,
            self.f)
        self.w0 = 0.5*np.concatenate(
            (np.ones([1,w.size]),-w[np.newaxis,:]),
            axis=0)

class CardioidSingle(Cardioid):
    def __init__(self, config=dict, inherit_par=dict):
        super().__init__(config=config, inherit_par=inherit_par)
        self.y = np.zeros([2,self.f.size],dtype=complex)

    def cycle(self, x: np.array) -> None:
        self.y = np.sum(self.w0 * x,axis=0,keepdims=True)

class CardioidDual(Cardioid):
    def __init__(self, config=dict, inherit_par=dict):
        super().__init__(config=config, inherit_par=inherit_par)
        self.y = np.zeros([2,self.f.size],dtype=complex)
        self.w1 = np.flipud(self.w0)

    def cycle(self, x: np.array) -> None:
        self.y[0,:] = np.sum(self.w0 * x,axis=0,keepdims=False)
        self.y[1,:] = np.sum(self.w1 * x,axis=0,keepdims=False)
