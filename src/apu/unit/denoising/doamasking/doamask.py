import numpy as np
import apu.apu as apu
import apu.nlt.native.logscl as lscl
import apu.nlt.native.mapping as mapping

class Masker(apu.Unit):
    def __init__(self, config=dict, inherit_par=dict):
        super().__init__()
        self.nchan = inherit_par['nchan']
        self.nband = inherit_par['nband']
        if config['enable']:
            self.g_inf = lscl.dB2amp(config['g_inf'])
        else:
            self.g_inf = 1
        
        self.start = inherit_par['tracked_bands'][0]
        self.stop = inherit_par['tracked_bands'][1]
        dg = 1 - self.g_inf
        self.k = config['passband_kp']/180*np.pi
        dk = np.maximum(np.spacing(1),config['stopband_kp']/180*np.pi - self.k)
        self.s = dg / dk

        self.g = np.ones([self.nchan, self.nband])

    def cycle(self, doa=np.array) -> None:
        self.g[:,self.start:self.stop] = mapping.zmap(
            x=doa[self.start:self.stop],
            xk=self.k,
            s=self.s,
            ymin=self.g_inf)
