import numpy as np
import apu.apu as apu
import apu.nlt.native.logscl as lscl

class Core(apu.Unit):
    def __init__(self, config=dict, inherit_par=dict):
        super().__init__()
        self.nchan = inherit_par['nchan']
        self.nband = inherit_par['nband']
        self.g = np.ones([self.nchan, self.nband])
        self.g_inf = lscl.dB2amp(config['g_inf'])

    def cycle(self, *gains) -> None:
        self.g = np.ones([self.nchan, self.nband])
        for g in gains:
            self.g = self.g * g
        self.g = np.maximum(self.g,self.g_inf)
