import numpy as np
import apu.apu as apu

class Core(apu.Unit):
    def __init__(self, config=dict, inherit_par=dict):
        super().__init__()
        self.nchan = inherit_par['nchan']
        self.nband = inherit_par['nband']
        self.y = np.zeros([self.nchan, self.nband])

    def cycle(self, x=np.array, g=np.array) -> None:
        self.y = g*x
