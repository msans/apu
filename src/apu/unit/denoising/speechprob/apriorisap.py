import numpy as np
import apu.apu as apu
import apu.unit.bandrouting.coupling as coupling
import apu.unit.bandrouting.distribution as distribution
import apu.nlt.native.logscl as lscl
import apu.unit.tracking.recursive as recursive

class NonLinSoftDecisionAdaptiveTracking:
    """Compute the instantaneous a-priori speech absence probability using 
    a non-linear soft-decision with adaptive tracking as in ref.

    Ref:
        CHOI, Min-Seok et KANG, Hong-Goo.
        An improved estimation of a priori speech absence probability for speech enhancement: in perspective of speech
        perception.
        In : Proceedings.(ICASSP'05). IEEE International Conference on Acoustics, Speech, and Signal Processing,
        2005. IEEE, 2005. p. I/1117-I/1120 Vol. 1.
    """

    def __init__(self, config=dict, inherit_par=dict):
        super().__init__()

        self.coupler = coupling.Coupling(config=config['_sub_coupling'])
        self.distributor = distribution.Distribution(config=config['_sub_distribution'])

        self.nchan = inherit_par['nchan']
        self.nband = inherit_par['nband']

        cfg = config['_sub_priosap']
        self.gamma_t_bb = lscl.dB2pow(np.maximum(0,cfg['postsnr_thresh_broadband']))
        self.gamma_t_sb = lscl.dB2pow(np.maximum(0,np.array(cfg['postsnr_thresh_subband'])))
        self.m = np.maximum(0, np.minimum(1, cfg['mixture_factor']))
        self.bb_median = cfg['bb_median']
        self.rec_smooth = cfg['rec_smoothing']
        self.ipriosap = np.zeros([self.nchan,self.coupler.nband])
        self.priosap = np.zeros([self.nchan,self.nband])
        self.a = np.ones([self.nchan,self.coupler.nband])

        sub_inherit_par = {
            'sampling_rate': inherit_par['sampling_rate'],
            'nchan': inherit_par['nchan'],
            'nband': self.coupler.nband,
        }
        self.track_priosap = recursive.TrackerDriven(
        {},
        inherit_par=sub_inherit_par)

    def cycle(self, gamma: np.array) -> None:

        # broadband post snr
        if self.bb_median:
            gamma_bb = np.median(gamma)
        else:
            gamma_bb = gamma.mean()
        iapsap_bb = self.gamma_t_bb / (self.gamma_t_bb + gamma_bb) # broadband dependent inst. priosap

        self.coupler.cycle(x=gamma)  
        gamma_mix = gamma_bb * self.m + self.coupler.y * (1 - self.m)
        iapsap_sb = self.gamma_t_sb / (self.gamma_t_sb + gamma_mix) 
        
        ipriosap = iapsap_bb + iapsap_sb - iapsap_bb*iapsap_sb # a+b-ab=a+b(1-a)=b+a(1-b)
        ipriosap = np.maximum(np.spacing(1), np.minimum(1-np.spacing(1), ipriosap)) 

        if self.rec_smooth:
            self.a = np.abs(np.sin(np.pi/2*(ipriosap-self.track_priosap.y)))

        self.track_priosap.cycle(x=ipriosap,a=self.a)

        self.distributor.cycle(x=ipriosap)
        self.ipriosap = self.distributor.y

        self.distributor.cycle(x=self.track_priosap.y)
        self.priosap = self.distributor.y
