import numpy as np
import apu.apu as apu
import apu.nlt.native.logscl as lscl
import apu.unit.denoising.priosnr.native as priosnr_native
import apu.unit.denoising.se.native as se_native

class MagnSpecSub(apu.Unit):
    def __init__(self, config=dict, inherit_par=dict):
        super().__init__()

        # speech estimator
        self.se = se_native.MagnSpecSub(
            config=config['_sub_se'],
            inherit_par=inherit_par)

        self.g = self.se.g
    
    def cycle(self, psd_x=np.array, lambda_d=np.array) -> None:
        
        self.se.cycle(psd_x=psd_x, lambda_d=lambda_d)
        self.g = self.se.g  

class ImlPowSpecSub(apu.Unit):
    def __init__(self, config=dict, inherit_par=dict):
        super().__init__()

        # priosnr
        self.priosnr = priosnr_native.Iml(
            config=config['_sub_priosnr'],
            inherit_par=inherit_par)

        # speech estimator
        self.se = se_native.PowSpecSub(
            config=config['_sub_se'],
            inherit_par=inherit_par)

        self.g = self.se.g
    
    def cycle(self, gamma=np.array) -> None:
        
        self.priosnr.cycle(gamma=gamma)
        self.se.cycle(ksi=self.priosnr.ksi_hat)
        self.g = self.se.g

class ImlWiener(apu.Unit):
    def __init__(self, config=dict, inherit_par=dict):
        super().__init__()

        # priosnr
        self.priosnr = priosnr_native.Iml(
            config=config['_sub_priosnr'],
            inherit_par=inherit_par)

        # speech estimator
        self.se = se_native.Wiener(
            config=config['_sub_se'],
            inherit_par=inherit_par)

        self.g = self.se.g
    
    def cycle(self, gamma=np.array) -> None:
        
        self.priosnr.cycle(gamma=gamma)
        self.se.cycle(ksi=self.priosnr.ksi_hat)
        self.g = self.se.g
