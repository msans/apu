import numpy as np
import apu.apu as apu
import apu.nlt.native.logscl as lscl
import apu.unit.denoising.priosnr.native as priosnr_native
import apu.unit.denoising.se.native as se_native

class DdOmStSa(apu.Unit):
    def __init__(self, config=dict, inherit_par=dict):
        super().__init__()

        # priosnr
        self.priosnr = priosnr_native.Dd(
            config=config['_sub_priosnr'],
            inherit_par=inherit_par)

        self.se = se_native.OmStSa(
            config=config['_sub_se'],
            inherit_par=inherit_par)

        self.g = self.se.g
    
    def cycle(self, gamma=np.array, q=np.array) -> None:
        
        self.priosnr.cycle(gamma=gamma, g=self.g)
        self.se.cycle(gamma=gamma, ksi=self.priosnr.ksi_hat, q=q)
        self.g = self.se.g

class DdOmStLsa(apu.Unit):
    def __init__(self, config=dict, inherit_par=dict):
        super().__init__()

        # priosnr
        self.priosnr = priosnr_native.Dd(
            config=config['_sub_priosnr'],
            inherit_par=inherit_par)

        self.se = se_native.OmStLsa(
            config=config['_sub_se'],
            inherit_par=inherit_par)

        self.g = self.se.g
    
    def cycle(self, gamma=np.array, q=np.array) -> None:
        
        self.priosnr.cycle(gamma=gamma, g=self.g)
        self.se.cycle(gamma=gamma, ksi=self.priosnr.ksi_hat, q=q)
        self.g = self.se.g
