import numpy as np
import apu.apu as apu
import apu.nlt.native.logscl as lscl
import apu.unit.denoising.priosnr.approx.pssa as priosnr_pssa
import apu.unit.denoising.se.native as se_native

class Pssa(apu.Unit):
    def __init__(self, config=dict, inherit_par=dict,variant=str):
        super().__init__()

        # priosnr
        self.priosnr = priosnr_pssa.Pssa(
            config=config['_sub_priosnr'],
            inherit_par=inherit_par)
        
        # speech estimator
        if variant == 'warpedwiener':
            self.se = se_native.WarpedWiener(
                config=config['_sub_se'],
                inherit_par=inherit_par)
        elif variant == 'omstsa':
            self.se = se_native.OmStSa(
                config=config['_sub_se'],
                inherit_par=inherit_par)
        elif variant == 'omstlsa':
            self.se = se_native.OmStLsa(
                config=config['_sub_se'],
                inherit_par=inherit_par)
        else:  # omwiener
            self.se = se_native.OmWiener(
                config=config['_sub_se'],
                inherit_par=inherit_par)

        self.g = self.se.g

    
    def cycle(self, gamma=np.array, q=np.array) -> None:
        
        self.priosnr.cycle(gamma=gamma)
        self.se.cycle(gamma=gamma, ksi=self.priosnr.ksi_hat, q=q)
        self.g = self.se.g

class PssaOmWiener(Pssa):
    def __init__(self, config=dict, inherit_par=dict):
        super().__init__(config=config,inherit_par=inherit_par,variant='omwiener')

class PssaOmStSa(Pssa):
    def __init__(self, config=dict, inherit_par=dict):
        super().__init__(config=config,inherit_par=inherit_par,variant='omstsa')

class PssaOmStLsa(Pssa):
    def __init__(self, config=dict, inherit_par=dict):
        super().__init__(config=config,inherit_par=inherit_par,variant='omstlsa')

class PssaWarpedWiener(Pssa):
    def __init__(self, config=dict, inherit_par=dict):
        super().__init__(config=config,inherit_par=inherit_par,variant='warpedwiener')
