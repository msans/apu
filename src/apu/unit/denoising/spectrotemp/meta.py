
import numpy as np
import apu.apu as apu
import apu.unit.denoising.spectrotemp.em as em
import apu.unit.denoising.spectrotemp.noreg as noreg
import apu.unit.denoising.spectrotemp.approx.pssa as pssa

class Wrap(apu.Unit):
    def __init__(self, config=dict, inherit_par=dict):
        super().__init__()
        
        magn_based_methods = set([
            'mss',
            ])
        priosap_based_methods = set([
            'dd_omstsa',
            'dd_omstlsa',
            'pssa_omwiener',
            'pssa_warpedwiener',
            'pssa_omstsa',
            'pssa_omstlsa',
            ])
        
        self.magn_based = False
        self.priosap_based = False
        if config['_method_label'] in magn_based_methods:
            self.magn_based = True
        elif config['_method_label'] in priosap_based_methods:
            self.priosap_based = True

        self.method = config['_method_label']
        self.methods = {
            'mss': noreg.MagnSpecSub,
            'iml_pss': noreg.ImlPowSpecSub,
            'iml_wiener': noreg.ImlWiener, 
            'dd_omstsa': em.DdOmStSa,
            'dd_omstlsa': em.DdOmStLsa,
            'pssa_omwiener': pssa.PssaOmWiener,
            'pssa_omstsa': pssa.PssaOmStSa,
            'pssa_omstlsa': pssa.PssaOmStLsa,
            'pssa_warpedwiener': pssa.PssaWarpedWiener,
        }
        self.Estimator = self.methods[self.method](
            config=config,
            inherit_par=inherit_par,
            )
        
    def cycle(self,
                gamma=np.array,
                q=np.array,
                psd_x=np.array,
                lambda_d=np.array) -> None:
        if self.magn_based:
            self.Estimator.cycle(psd_x=psd_x, lambda_d=lambda_d)
        elif self.priosap_based:
            self.Estimator.cycle(gamma=gamma,q=q)
        else:
            self.Estimator.cycle(gamma=gamma) 
