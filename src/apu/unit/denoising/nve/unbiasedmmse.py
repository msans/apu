import numpy as np
import apu.apu as apu
import apu.nlt.native.timeconst as tc
import apu.nlt.native.logscl as lscl
import apu.unit.tracking.recursive as recursive

class UnBiasedMmse(apu.Unit):
    """Estimate noise power from noisy signal PSD
    Ref:
    [1]:
        GERKMANN, Timo et HENDRIKS, Richard C.
        Unbiased MMSE-based noise power estimation with low complexity and low tracking delay.
        IEEE Transactions on Audio, Speech, and Language Processing, 2011, vol. 20, no 4, p. 1383-1393.
    [2]:
        GERKMANN, Timo et HENDRIKS, Richard C.
        Noise power estimation based on the probability of speech presence.
        In : 2011 IEEE Workshop on Applications of Signal Processing to Audio and Acoustics (WASPAA). 
        IEEE, 2011. p. 145-148.
    """
    def __init__(self, config=dict, inherit_par=dict):
        super().__init__()

        self.sr = inherit_par['sampling_rate']
        self.nchan = inherit_par['nchan']
        self.nband = inherit_par['nband']

        # cssp parameters
        self.ksi_H1_typical_obs = lscl.dB2pow(config["ksi_H1_typical_obs"])
        self.gw_H1_typical_obs = self.ksi_H1_typical_obs / (self.ksi_H1_typical_obs + 1) # wiener gain if H1

        # cspp post-prcessing parameters
        self.cspp_sup = config["cspp_sup"]
        self.alpha_cspp = tc.t2a(config["tc_cspp"], self.sr)

        # nve parameters
        self.alpha_nve = tc.t2a(config['tc_nve'], self.sr) # noise periodogram time const.

        sub_inherit_par = {
            'sampling_rate': inherit_par['sampling_rate'],
            'nchan': inherit_par['nchan'],
            'nband': inherit_par['nband'],
        }
        
        self.track_cspp = recursive.TrackerDriven(
            {},inherit_par=sub_inherit_par)
        self.cspp = np.zeros([self.nchan, self.nband])  # post speech presence probability (cspp)

        self.track_psd_d = recursive.TrackerDriven(
            {},inherit_par=sub_inherit_par)
        self.track_lambda_d = recursive.TrackerDriven(
            {},inherit_par=sub_inherit_par)

        self.lambda_d = lscl.dB2pow(-70)*np.ones([self.nchan, self.nband])  # noise variance
        self.psd_d = lscl.dB2pow(-70)*np.ones([self.nchan, self.nband])  # noise psd


    def cycle(self, psd_y=np.array) -> None:

        # post SNR estimation
        # (based on (internal) nve from previous frame)
        #gamma = np.maximum(1,psd_y / np.maximum(np.spacing(1),self.lambda_d))  # biased estim
        gamma = psd_y / np.maximum(np.spacing(1),self.lambda_d)  # unbiased estim.

        # conditional speech presence probability p[H1|psd_y] 
        # (bayesian derivation under worst case equi-prob. assumption, i.e p[H1] = p[H0] = 1/2)
        # [2]: eq. (8) aka Algorithm 1 step 2
        cspp = 1 / (1 + (1 + self.ksi_H1_typical_obs) * np.exp(-gamma * self.gw_H1_typical_obs))

        # cspp post-prcessing: saturation
        # (protection against stagnation at high speech presence probability)
        # [2]: eq. (11)&(12) aka Algorithm 1 step 3&4
        self.track_cspp.cycle(x=cspp,a=self.alpha_cspp)
        self.smoothed_cspp = self.track_cspp.y
        f = self.smoothed_cspp > self.cspp_sup
        self.cspp = f*np.minimum(cspp,self.cspp_sup) + (1-f)*cspp

        # speech absence probaility under H1, csap = 1 - cspp
        csap = 1 - self.cspp  # P[H0|psd_y] = 1 - P[H1|psd_y]
        
        # [2]: eq(10) aka Algorithm 1 step 5
        # psd_d = csap * psd_y + self.cspp * self.lambda_d
        self.track_psd_d.cycle(x=psd_y,a=csap)
        self.psd_d = self.track_psd_d.y

        # above equ. can be combined with (to increase parallelism)
        # [2]: eq(2) aka Algorithm 1 step 6
        # self.lambda_d = self.alpha_nve * psd_d + (1-self.alpha_nve)*self.lambda_d
        # defining an adaptive time constant
        # self.lambda_d = self.alpha_nve * csap * psd_y + ... 
        #   (self.alpha_nve*(1-csap) + (1-self.alpha_nve))*self.lambda_d
        # conditional speech absence modulated smoothing coeff:
        
        self.a = self.alpha_nve * csap
        self.track_lambda_d.cycle(x=psd_y,a=self.a)
        self.lambda_d = self.track_lambda_d.y
        return None
