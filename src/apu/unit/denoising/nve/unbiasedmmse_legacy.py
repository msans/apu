import numpy as np
import apu.apu as apu
import apu.nlt.native.timeconst as tc
import apu.nlt.native.logscl as lscl

class UnBiasedMmse(apu.Unit):
    """Estimate noise power from noisy signal PSD
    Ref:
        GERKMANN, Timo et HENDRIKS, Richard C.
        Unbiased MMSE-based noise power estimation with low complexity and low tracking delay.
        IEEE Transactions on Audio, Speech, and Language Processing, 2011, vol. 20, no 4, p. 1383-1393.
    """
    def __init__(self, config=dict, inherit_par=dict):
        super().__init__()

        self.sr = inherit_par['sampling_rate']
        self.nchan = inherit_par['nchan']
        self.nband = inherit_par['nband']

        # internal constants
        self.postspp_ub = config["postspp_ub"]
        self.ksi_H1_typical_obs = lscl.dB2pow(config["ksi_H1_typical_obs"])
        self.gw_H1_typical_obs = self.ksi_H1_typical_obs / (self.ksi_H1_typical_obs + 1)  # wiener gain
        self.alpha_postspp = tc.t2a(config["tc_postspp"], self.sr)
        self.alpha_min = tc.t2a(config["tc_max"], self.sr)
        self.aT = config["accT"]
        self.adT = config["accdT"]

        # parameters
        self.alpha_npsd = tc.t2a(config['tc'], self.sr) # noise periodogram time const.

        # regsitry
        
        self.lambda_d = lscl.dB2pow(-70)*np.ones([self.nchan, self.nband])  # noise variance
        self.lambda_d_tilde = lscl.dB2pow(-70)*np.ones([self.nchan, self.nband])  # noise variance long term
        self.postspp = np.zeros([self.nchan, self.nband])  # post speech presence probability (postspp)
        self.spostspp = np.zeros([self.nchan, self.nband])  # smoothed postspp
        self.a = np.ones([self.nchan, self.nband])  # smoothed postspp

    def cycle(self, psd_y: np.array) -> None:

        gamma = psd_y / np.maximum(np.spacing(1),self.lambda_d_tilde)

        postspp = 1 / (1 + (1 + self.ksi_H1_typical_obs) * np.exp(-gamma * self.gw_H1_typical_obs))
        self.spostspp = self.alpha_postspp*postspp + (1-self.alpha_postspp) * self.spostspp
        f = self.spostspp > self.postspp_ub
        self.postspp = f*np.minimum(postspp,self.postspp_ub) + (1-f)*postspp
        postsap = (1 - self.postspp)

        # boot-strapp mecanismes
        d_dB = lscl.pow2dB(np.maximum(np.spacing(1),psd_y)) - lscl.pow2dB(np.maximum(np.spacing(1),self.lambda_d))
        f = np.minimum(1,np.maximum(0,d_dB - self.aT)/self.adT)
        da = self.alpha_min*f
        
        a = np.minimum(1,self.alpha_npsd * postsap + da)
        self.lambda_d_tilde = self.lambda_d_tilde + a * (psd_y - self.lambda_d_tilde)
        
        #self.lambda_d_tilde = self.lambda_d_tilde + self.alpha_npsd * postsap * (psd_y - self.lambda_d_tilde)
        
        ksi_ml = np.maximum(np.spacing(1), gamma - 1)  # prio snr, inst. ML
        spp = ksi_ml > 1
        self.a = self.alpha_npsd*(1-spp)
        self.lambda_d = self.lambda_d + self.a*(psd_y-self.lambda_d)

        return None
