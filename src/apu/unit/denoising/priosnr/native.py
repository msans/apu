import numpy as np
import apu.apu as apu
import apu.nlt.native.logscl as lscl


class PrioSnr(apu.Unit):
    def __init__(self, config=dict, inherit_par=dict):
        super().__init__()

        self.sr = inherit_par['sampling_rate']
        self.nchan = inherit_par['nchan']
        self.nband = inherit_par['nband']

        self.ksi_hat_inf = lscl.dB2pow(config['ksi_hat_inf'])
        self.ksi_hat = np.ones([self.nchan, self.nband])
    
class Iml(PrioSnr):
    """Prio SNR estimation using the instantaneous maximum likelihood prio SNR, i.e. no regularization"""
    def __init__(self, config=dict, inherit_par=dict):
        super().__init__(config=config, inherit_par=inherit_par)

    def cycle(self, gamma: np.array) -> None:
        self.ksi_hat =  np.maximum(self.ksi_hat_inf, np.maximum(0, gamma - 1))

class Dd(PrioSnr):
    """Prio SNR estimation using the decision directed approach
    Ref:
        EPHRAIM, Yariv et MALAH, David.
        Speech enhancement using a minimum-mean square error short-time spectral amplitude estimator.
        IEEE Transactions on acoustics, speech, and signal processing, 1984, vol. 32, no 6, p. 1109-1121.
    """

    def __init__(self, config=dict, inherit_par=dict):
        super().__init__(config=config, inherit_par=inherit_par)

        self.iml = Iml(config=config, inherit_par=inherit_par)

        self.beta = config['beta']
        self.gamma = np.ones([self.nchan, self.nband])

    def cycle(self, gamma: np.array, g: np.array) -> None:
        # ksi_ml[k](gamma[k])
        self.iml.cycle(gamma=gamma) 
        self.ksi_ml = self.iml.ksi_hat 
        # ksi_hat[k](g[k-1], gamma[k-1], ksi_ml[k]; beta)
        self.ksi_hat = np.maximum(self.ksi_hat_inf,
            self.beta * (g**2) * self.gamma + (1-self.beta) * self.ksi_ml)
        self.gamma = gamma # registry update
