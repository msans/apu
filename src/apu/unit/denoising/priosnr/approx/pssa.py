import numpy as np
import apu.apu as apu
import apu.nlt.native.logscl as lscl
import apu.nlt.native.activation as act
import apu.unit.denoising.priosnr.native as native



class Pssa(native.PrioSnr):
    """Prio SNR estimation using Parametric Shifted Softplus Approximation
    [1]:
        FIXME
    """

    def __init__(self, config=dict, inherit_par=dict):
        super().__init__(config=config, inherit_par=inherit_par)

        self.ksi_hat_dB = np.zeros([self.nchan, self.nband])

        # z(x,s) = h(s) + 1/b*softplus(b(x - x0(s)))
        # with
        # input: x (dksi_dB)
        # state: s (ksi_hat_prev_dB)
        # output: y (dksi_hat_dB)
        # and
        # h: post-shift
        # x0: pre-shift

        self.b = config['shifted_splus_bending']

        # post-shift parametrization
        # with
        # h = h_o + 1/c_o*softplus(s-x0_o)
        # with
        # input: s (ksi_hat_dB, state)
        # output: h (post-shift)
        self.c_o = config['postshift_splus_bending']
        self.x0_o = config['postshift_splus_inbias']
        self.h_o = config['postshift_splus_outbias']

        # pre-shift parametrization
        # x0 = h + dz0
        # dz0 = a_i*(1 - logistic(k_i*(x - x0_i))) + h_i
        # with
        # input: s (ksi_hat_dB, state)
        # output: x0 (pre-shift)
        self.x0_i = config['preshift_logistic_inbias']
        self.h_i = config['preshift_logistic_outbias']
        self.k_i = config['preshift_logistic_steepness']
        self.a_i = config['preshift_logistic_amp']

    def cycle(self, gamma: np.array) -> None:

        # pre-processing
        ksi_ml =  np.maximum(self.ksi_hat_inf, np.maximum(0,
            gamma - 1))

        ksi_ml_dB = lscl.pow2dB(ksi_ml)
        dksi_dB = ksi_ml_dB - self.ksi_hat_dB

        # state dependent parametrization
        h = -act.xsoftplus(
                x=self.ksi_hat_dB,
                x0=self.x0_o,
                b=self.c_o,
                a=1,
                h=-self.h_o)  # post-shift

        dz0 = self.h_i + self.a_i - act.xlogistic(
            x=self.ksi_hat_dB,
            x0=self.x0_i,
            k=self.k_i,
            a=self.a_i,
            h=0)  

        x0 = dz0 + h
        z = act.xsoftplus(dksi_dB,x0,self.b,1,h)
        
        self.ksi_hat_dB = self.ksi_hat_dB + z
        
        # post-processing
        self.ksi_hat = lscl.dB2pow(self.ksi_hat_dB)
