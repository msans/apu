import numpy as np
from scipy import fft
from scipy import signal as sp_signal
import apu.apu as apu

def get_cola_pr_hop_sizes(window: np.array) -> list:
    cola_pr_hop_sizes = list()
    M = window.size  # window length
    for R in np.arange(1,M+1,1):
        pdwg = np.zeros(R) # gain of polyphase decomposed window
        for r in np.arange(0,R,1):   # test all polyphase decompositions
            wp = window[r::R]
            pdwg[r] = wp.sum()
        if np.all(pdwg < pdwg[0] + M*np.spacing(1)) and np.all(pdwg > pdwg[0] - M*np.spacing(1)):
            cola_pr_hop_sizes.append(R)
    return cola_pr_hop_sizes

def is_cola_pr(window: np.array, hop_size: int) -> bool:
    M = window.size # window length
    R = hop_size    # hop size aka downsampling factor
    N = 3*M         #  Overlap-Add span
    y = np.zeros(N+R)  # len N, with + R safety samples
    for q in np.arange(0, N-M+R, R):
        y[q:q+M] += window
    x = y[M:2*M]
    return np.all(x > x[0] - M*np.spacing(1)) and np.all(x < x[0] + M*np.spacing(1))

def get_windows(
            awindow: tuple,
            nawin: int,
            swindow: tuple, 
            nswin: int,
            nfft: int,
            nframe: int,
            sampling_frequency: int, 
            aexp=1, 
            sexp=1, 
            norm_sbya=False) -> list:

    # FIXME: conditions
    #  - nfft must be a power of two
    #  - nfft must be an positive integer multiple of nframe, i.e. k = nfft/nframe with k>=0 and integer
    #  - nawin must be <= nfft (TO BE CHECKED: could we allow nawin > nfft ?)
    #  - nswin must be <= nfft (TO BE CHECKED: could we allow nswin > nfft ?)
    down_sample_freq = sampling_frequency / nframe

    zending_symwin = tuple(['hann', 'bartlett', 'bohman', 'barthann', 'tukey'])

    # analysis window
    if norm_sbya:
        N = nawin
        if awindow[0] in zending_symwin:
            N += 2
        wa = sp_signal.get_window(awindow, N, fftbins=False)  # symmetric
        if awindow[0] in zending_symwin:
            wa = wa[1:-1]
    else:
        wa = np.power(sp_signal.get_window(awindow, nawin, fftbins=True),aexp)  # periodic
    wa = np.concatenate((wa, np.zeros( nfft - nawin)))

    # synthesis window
    ws = np.power(sp_signal.get_window(swindow, nswin, fftbins=True), sexp)  # periodic
    if norm_sbya:
        ws = ws/wa[nfft-nswin:]
    ws = np.concatenate((np.zeros(nfft - nswin), ws))   # -> l-side zp (output side of the ola buffer)
                                                        # with nswin = k+nframe + reisudal, with k pos. int
                                                        # k frames can be skipped by the ola process to decrease the
                                                        # delay

    cola_check = sp_signal.check_COLA(wa*ws, nfft, nfft-nframe, tol=1e-10)
    nola_check = sp_signal.check_NOLA(wa*ws, nfft, nfft-nframe, tol=1e-10)

    return down_sample_freq, wa, ws, cola_check, nola_check


class Wola(apu.Unit):
    def __init__(self, config=dict, inherit_par=dict):
        super().__init__()
        
        self.sampling_rate = inherit_par['sampling_rate']
        self.nchan = inherit_par['nchan']

        self.N = config['fft_len']  #  FFT size
        self.D = inherit_par['frame_len']   # hop size 
                                            # aka downsampl. factor 
                                            # aka time frame size
        self.Lwa = config['awin_len']  # analys. win size (w/o zero pad.)
        self.Lws = config['swin_len']  # synthes. win size (w/o zero pad.)

        down_sample_freq, wa, ws, cola_check, nola_check = get_windows(
            tuple(config["awin"]),
            self.Lwa,
            tuple(config["swin"]),
            self.Lws,
            self.N,
            self.D,
            self.sampling_rate,
            config["aexp"],
            config["sexp"],
            config["norm_sbya"])

        self.cola_check = cola_check  #  Constant OverLap Add (COLA) constraint 
        self.nola_check = nola_check  #  Nonzero Overlap Add (NOLA) constraint
        self.down_sample_freq = down_sample_freq
        self.wa = wa  # analysis window  (already zero-padded to N)
        self.ws = ws  # synthesis window (already zero-padded to N)

        self.M = int(self.N / 2)    # number of bins for single-sided spectrum
        
        self.Lxa = int(np.ceil(self.Lwa / self.D)) * self.D  # analysis buffer size
        self.Lxs = int(np.ceil(self.Lws / self.D)) * self.D  # synthesis buffer size
        self.xa = np.zeros([self.nchan, self.Lxa])  # analysis buffer
        self.xs = np.zeros([self.nchan, self.Lxs])  # synthesis buffer

        

        self.Gw = (self.wa * self.ws).sum()  # analysis-synthesis window gain
        self.Ga = 1.0  # analysis window normalizing gain
        self.Gs = self.D / self.N # synthesis window normalizing gain (D/N: number of overlapped segment)

        self.norm = config['norm']
        match self.norm:
            case 'forward':
                self.Gwa = self.Gw
                self.Gws = 1
            case 'ortho':  # is actually ortho if wa=ws, otherwise it means total DC gain equally distibuted
                self.Gwa = np.sqrt(self.Gw)
                self.Gws = self.Gwa
            case 'backward':
                self.Gwa = 1
                self.Gws = self.Gw
            case other:
                raise (ValueError)
        self.Ewa = (self.wa * self.wa / np.power(self.Gwa, 2)).sum()
        self.Ews = (self.ws * self.ws / np.power(self.Gws, 2)).sum()
        self.Ga = self.Ga / self.Gwa  # normalize by window gain
        self.Gs = self.Gs / self.Gws  # normalize by window gain
        self.norm_single_sided_spectrum = config['norm_single_sided_spectrum']
        if self.norm_single_sided_spectrum:
            self.Ga = self.Ga * np.sqrt(2)  # apply a power factor 2 on the single-sided spectrum
            self.Gs = self.Gs / np.sqrt(2)  # remove the power factor 2 applied on the single-sided spectrum
            self.Ewa = self.Ewa*2
            self.Ews = self.Ews/2

class WolaFwd(Wola):
    def __init__(self, config=dict, inherit_par=dict):
        super().__init__(config=config, inherit_par=inherit_par)
        self.X = np.zeros([self.nchan, self.M]) 

    def cycle(self,x=np.array) -> None:
        
        self.xa = np.roll(self.xa, -self.D, axis=-1) # update delay line: cycle shift ...
        self.xa[:,-self.D:] = x # ... then overwrite
        y = np.concatenate((self.xa, np.zeros([self.nchan, self.N - self.Lxa])), axis=-1)

        # forward DFT
        Xds = fft.fft(self.wa * y, n=self.N, norm="backward", axis=-1)  # avoid any normaliz. by the scipy.fft.fft

        # convert to single-sided spectrum (half of the power is dropped)
        Xdc = 1 / 2 * np.real(Xds[:,0])  # DC component (pure real)
        Xnq = 1 / 2 * np.real(Xds[:,self.M])  # Nyquist freq. component (pure real because N even)
        X = np.zeros([self.nchan,self.M]) + 1j * np.zeros([self.nchan,self.M])  # single-sided spectrum
        X.imag[:,0] = (Xdc - Xnq)
        X.real[:,0] = Xdc + Xnq
        X[:,1:] = Xds[:,1:self.M]
        self.X = self.Ga * X  # apply desired normalization and return
        

class WolaBwd(Wola):
    def __init__(self, config=dict, inherit_par=dict):
        super().__init__(config=config, inherit_par=inherit_par)
        self.x = np.zeros([self.nchan, self.D]) 
    
    def cycle(self,X=np.array) -> None:
        self.xs = np.roll(self.xs, -self.D, axis=-1)  # update delay line: cycle shift ...
        self.xs[:,-self.D:] = np.zeros([self.nchan, self.D])  # ... then overwrite)

        # convert back to double-sided spectrum (power is doubled, back to normal in this process)
        #reX0 = np.real(X[:,0])[:,np.newaxis]
        #imX0 = np.imag(X[:,0])[:,np.newaxis]
        reX0 = np.real(X[:,0])
        imX0 = np.imag(X[:,0])
        Xds = np.zeros([self.nchan, self.N]) + 1j * np.zeros([self.nchan, self.N])  # double-sided spectrum
        Xds[:,0] = np.real(reX0 + imX0) + 1j * np.zeros(self.nchan)  # DC is real
        Xds[:,1:self.M] = X[:,1:]
        Xds[:,self.M] = np.real(reX0 - imX0) + 1j * np.zeros(self.nchan)  # Nyquist freq. comp. is real because fft len is even
        Xds[:,self.M+1:] = np.flip(np.conjugate(X[:,1:]), axis=-1)

        # backward DFT (IDFT)
        y = self.ws*np.real(fft.ifft(Xds, n=self.N, norm="forward", axis=-1))  # avoid any normaliz. by the scipy.fft.ifft
        self.xs += y[:,-self.Lxs:]
        self.x = self.Gs * self.xs[:,0:self.D] # apply desired normalization and return
