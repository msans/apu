import numpy as np

def dB2pow(x: np.array) -> np.array:
    return np.power(10, x / 10)

def pow2dB(x: np.array) -> np.array:
    return 10 * np.log10(np.maximum(np.spacing(1), x))

def dB2amp(x: np.array) -> np.array:
    return np.power(10, x / 20)

def amp2dB(x: np.array) -> np.array:
    return 20 * np.log10(np.maximum(np.spacing(1), x))
