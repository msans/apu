import numpy as np

def delaying_phasor(d: float, theta: float, c: float, f: np.array) -> np.array:
    """ Return delaying phasor
    :param d: inter-mic distance [m]
    :param theta: direction of arrival [rad]
    :param c: sound speed m/s
    :param f: frequency [Hz]
    """
    return np.exp(-2*np.pi*1j*f/c*d*np.cos(theta))
