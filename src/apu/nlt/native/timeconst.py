import numpy as np

def t2b(t: np.array, sr: float) -> np.array:
    return np.exp(-1 / sr / (np.maximum(np.spacing(1), t)))

def t2a(t: np.array, sr: float) -> np.array:
    return 1 - t2b(t, sr)

def b2t(b: np.array, sr: float) -> np.array:
    return -1/sr/np.log(b)

def a2t(a: np.array, sr: float) -> np.array:
    return b2t(1-a,sr)
