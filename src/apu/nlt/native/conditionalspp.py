import numpy as np
import apu.nlt.native.se as se
import apu.nlt.native.activation as act

def gaussian_model(
        gamma=np.array, 
        ksi=np.array, 
        q=np.array,
        ) -> (np.array, np.array, np.array,):
    """Compute conditional speech presence probability under gaussian assumption
    :param gamma: post snr
    :param ksi: prio snr
    :param q: a-priori speech absence probability

    [1]:
        EPHRAIM, Yariv et MALAH, David.
        Speech enhancement using a minimum-mean square error short-time spectral amplitude estimator.
        IEEE Transactions on acoustics, speech, and signal processing, 1984, vol. 32, no 6, p. 1109-1121.
    """
    gw = se.wiener(ksi)  # wiener gain
    nu = gamma*gw
    nu_exp = np.exp(-nu)
    p = 1 / (1 + q/(1-q)*(1 + ksi) * nu_exp)
    p =  np.maximum(np.spacing(1), np.minimum(1-np.spacing(1), p))
    return p, nu, gw

def warped_gaussian_model(
        gamma=np.array, 
        ksi=np.array, 
        q=np.array,
        k=np.array,
        x0=np.array,
        ) -> (np.array, np.array, np.array,):
    
    gw = se.wiener(ksi)  # wiener gain
    nu = gamma*gw
    # warped logit of cspp
    x = nu + np.log(1+ksi) + np.log(q/(1-q))
    p = act.xlogistic(x=x,x0=x0,k=k,a=1,h=0) # wapred cspp
    p =  np.maximum(np.spacing(1), np.minimum(1-np.spacing(1), p))
    return p, nu, gw
