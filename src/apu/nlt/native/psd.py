import numpy as np

def auto(x: np.array) -> np.array:
    """Computes Auto-PSD
    :param x: input spectrum
    """
    return np.real(np.conjugate(x)*x)

def cross(x: np.array, y: np.array) -> np.array:
    """Computes Cross-PSD
    :param y: input spectrum
    :param x: input spectrum
    """
    return np.conjugate(y)*x

def cross_dualchan(x: np.array) -> np.array:
    """Computes Cross-PSD
    :param x: input spectrum, dual channel
    """
    return np.conjugate(x[1,:])*x[0,:]
