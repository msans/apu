import numpy as np
import math
import scipy.special as spsp

gf_1dot5 = math.gamma(1.5)

def magnspecsub(psd_x=np.array,lambda_d=np.array):
    """Magnitude spectral subtraction speech estimator
    :param psd_x: observed noisy speech psd
    :param lambda_d: noise variance estimate
    :param ksi_ml_min: minimum allowed value for the maximum likelihood a-priori snr

    [1]:
        BOLL, Steven.
        Suppression of acoustic noise in speech using spectral subtraction.
        IEEE Transactions on acoustics, speech, and signal processing, 1979, vol. 27, no 2, p. 113-120.
    """
    magn_x = np.sqrt(psd_x)
    magn_d = np.sqrt(np.maximum(0,lambda_d))
    return np.maximum(0,magn_x - magn_d) / np.maximum(np.spacing(1),magn_x) # magn_s / magn_x = magn_s / (magn_s + magn_d) 

def wiener(ksi=np.array):
    """Wiener speech estimator
    :param ksi: prio SNR

    [1]:
        WIENER, Norbert.
        Extrapolation, interpolation, and smoothing of stationary time series: with engineering applications.
        The MIT press, 1949.
    """
    return ksi/(ksi + 1)  # psd_s/psd_x = psd_s / (psd_s + lambda_d)

def powspecsub(ksi=np.array):
    """Power spectral subtraction speech estimator aka instantaneous (i.e. averaged on 1 frame) maximum-likelihood SNR
    estimator
    :param ksi: prio SNR

    [1]:
        LIM, Jae Soo et OPPENHEIM, Alan V.
        Enhancement and bandwidth compression of noisy speech.
        Proceedings of the IEEE, 1979, vol. 67, no 12, p. 1586-1604.
    """
    return np.sqrt(wiener(ksi=ksi)) # sqrt(psd_s/psd_x) = sqrt(psd_s / (psd_s + lambda_d))

def sa(gamma=np.array, nu=np.array) -> np.array:
    """Short-Time Spectral Amplitude speech estimator
    [1]:
        EPHRAIM, Yariv et MALAH, David.
        Speech enhancement using a minimum-mean square error short-time spectral amplitude estimator.
        IEEE Transactions on acoustics, speech, and signal processing, 1984, vol. 32, no 6, p. 1109-1121.
    """
    sqrt_nu = np.sqrt(nu)
    nu_over2 = nu/2
    exp_nu = np.exp(-nu_over2)
    i0 = spsp.i0(nu_over2)
    i1 = spsp.i1(nu_over2)
    return gf_1dot5 * sqrt_nu / gamma * exp_nu * ((1 + nu) * i0 + nu * i1)

def lsa(nu=np.array, gw=np.array) -> np.array:
    """Short-Time Log Spectral Amplitude speech estimator
    [1]:
        EPHRAIM, Yariv et MALAH, David.
        Speech enhancement using a minimum mean-square error log-spectral amplitude estimator.
        IEEE transactions on acoustics, speech, and signal processing, 1985, vol. 33, no 2, p. 443-445.
    """
    return gw * np.exp(0.5 * spsp.expn(1, np.maximum(np.spacing(1), nu)))
