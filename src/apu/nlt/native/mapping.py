import numpy as np

def zmap(x=np.array, xk=np.array, s=np.array, ymin=np.array) -> np.array:
    """Fully parametric z-shape mapping rule
    :param x: input
    :param xk: input knee-point
    :param s: slope
    :param ymin: minimal output
    """
    return np.maximum(ymin,np.minimum(1,1 - (x-xk)*s))

def fzmap(x=np.array, xk=np.array, s=np.array, ymin=np.array) -> np.array:
    """Fully parametric flipped-z-shape mapping rule
    :param x: input
    :param xk: input knee-point
    :param s: slope
    :param ymin: minimal output
    """
    return np.maximum(ymin,np.minimum(1,(x-xk)*s+ymin))
