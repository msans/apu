# !/usr/bin/env python3
# -*- coding: utf-8 -*-

import numpy as np

def softplus(x: np.array) -> np.array:
    return np.log(1+np.exp(x))

def xsoftplus(
    x=np.array,
    x0=np.array,
    b=np.array,
    a=np.array,
    h=np.array,
    ) -> np.array:
    np.log(1+np.exp(b*(x-x0)))
    return a/b*softplus(b*(x-x0))+h

import numpy as np

def logistic(x: np.array) -> np.array:
    return 1/(1+np.exp(-x))

def xlogistic(
    x=np.array,
    x0=np.array,
    k=np.array,
    a=np.array,
    h=np.array,
    )-> np.array:
    return a*logistic(k * (x - x0)) + h
