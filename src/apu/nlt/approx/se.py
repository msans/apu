import numpy as np
import math
import scipy.special as spsp

def lsa(nu=np.array, gw=np.array) -> np.array:    
    """Short-Time Log Spectral Amplitude speech estimator Approximation
    :param gamma: post SNR
    :param ksi: prio SNR (estimate)

    [1]:
        BENESTY, Jacob, CHEN, Jingdong, HUANG, Yiteng, et al.
        Noise reduction in speech processing.
        Springer Science & Business Media, 2009.self.p
    """
    f0 = nu < 0.1
    f1 = nu > 1.0
    nu_log = np.log10(np.maximum(np.spacing(1), nu))
    y0 = -2.31 * nu_log - 0.6
    y1 = -1.544 * nu_log + 0.166
    y2 = 10**(-0.52 * nu - 0.26)
    return gw * np.exp(1/2*(f0*y0 + (1-f0)*(1-f1)*y1 + f1*y2))
