import pathlib
from abc import ABC, abstractmethod
import numpy as np
import apu.utils.configurator as configurator
import apu.utils.io as io

class App(ABC):
    def __init__(self,config_path=pathlib.Path):
        
        self.unit = None

        p = config_path.absolute()
        #  get app configuration
        self.app_conf = configurator.read_conf(
            config_file=str(p.joinpath('io.toml')))
        
        # audio processing unit configuration
        unit_configreader = configurator.ConfigReader(p.joinpath('config_file_units.toml'))
        self.unit_conf = unit_configreader.get()

        # i/o sources configuration
        self.nframes = None
        self.duration = None
        self.input_dir = configurator.get_path(
            path_file=self.app_conf['input']['path_file'])
        if self.app_conf['output']['store']:
            self.output_dir = configurator.get_path(
                path_file=self.app_conf['output']['path_file'])

    @abstractmethod
    def io_init(self) -> None:
        pass

    @abstractmethod
    def run(self) -> None:
        pass


class FullFileProcessingApp(App):
    def __init__(self,config_path=pathlib.Path):
        super().__init__(config_path=config_path)
        self.input_files = list()
        self.output_files = list()
        self.nframes = 0

        if self.app_conf['input']['components_enabled']:
            
            self.components_dir = configurator.get_path(
                path_file=self.app_conf['components']['path_file'])

            self.components_files = {}
            for component in self.app_conf['components']['file_prefix']:
                self.components_files[component] = list()
            
            
    def io_init(self,output=False) -> None:
        self._o_init() if output else self._i_init()

    def _i_init(self):
        
        parsubset = ('sampling_rate', 'frame_len')
        self.input_files, self.nframes = io.open_input_files(
            source_path=self.input_dir,
            unit_config={k: self.unit_conf[k] for k in parsubset},
            app_config=self.app_conf['input'])

        self.duration = self.nframes*self.unit_conf['frame_len']

        self.mics = np.zeros(
            [self.app_conf['input']['nchan'],self.duration],
            dtype=self.unit_conf['dtype'])
        self.recs = np.zeros(
            [self.app_conf['output']['nchan'],self.duration],
            dtype=self.unit_conf['dtype'])

        for k in range(self.app_conf['input']['nchan']):
            self.mics[k,:] = self.input_files[k].read(
                frames=self.duration,
                dtype=self.unit_conf['dtype'],
                always_2d=False,
                fill_value=None,
                out=None)

        [f.close for f in self.input_files]

        if self.app_conf['input']['components_enabled']:
            self.components = {}
            app_conf = {
                'path_file': self.app_conf['input']['path_file'],
                'format': self.app_conf['input']['format'],
                'subtype': self.app_conf['input']['subtype'],
                'nchan': self.app_conf['input']['nchan'],
                'file_prefix': '',
            }
            for component in self.app_conf['components']['file_prefix']:
                app_conf['file_prefix'] = component
                self.components_files[component], nframes = io.open_input_files(
                    source_path=self.components_dir,
                    unit_config={k: self.unit_conf[k] for k in parsubset},
                    app_config=app_conf)

                if self.nframes != nframes:
                    raise ApuInputError(" ".join([
                    f"Component files {component}: Expected {self.nframes} frames",
                    f", found {nframes}"]))

                self.components[component] = np.zeros(
                    [app_conf['nchan'],self.duration],
                    dtype=self.unit_conf['dtype'])

                for k in range(app_conf['nchan']):
                    self.components[component][k,:] = self.components_files[component][k].read(
                        frames=self.duration,
                        dtype=self.unit_conf['dtype'],
                        always_2d=False,
                        fill_value=None,
                        out=None)

                [f.close for f in self.components_files[component]]

        return None

    def _o_init(self) -> None:

        if not(self.app_conf['output']['store']):
            return None

        parsubset = ('sampling_rate', 'frame_len')
        self.output_files = io.open_output_files(
            source_path=self.output_dir,
            unit_config={k: self.unit_conf[k] for k in parsubset},
            app_config=self.app_conf['output'])

        for k, f in enumerate(self.output_files):
            f.write(self.recs[k])
            f.close 
        
        return None

    def run(self) -> None:
        
        for n in range(self.nframes):
            i0 = n*self.unit_conf['frame_len']
            i1 = i0 + self.unit_conf['frame_len']
            self.unit.cycle(data=self.mics[:,i0:i1])
            self.recs[:,i0:i1] = self.unit.out
            if n == self.nframes:
                break

        self.io_init(True)

        return None

class ChunkedFileProcessingApp(App):
    def __init__(self,config_path=pathlib.Path):
        super().__init__(config_path=config_path)
        self.input_files = list()
        self.output_files = list()
        self.nframes = 0
        
    def io_init(self) -> None:
        parsubset = ('sampling_rate', 'frame_len')
    
        self.input_files, self.nframes = io.open_input_files(
            source_path=self.input_dir,
            unit_config={k: self.unit_conf[k] for k in parsubset},
            app_config=self.app_conf['input'])

        self.duration = self.nframes*self.unit_conf['frame_len']

        self.mics = list()
        for k in range(self.app_conf['input']['nchan']):
            self.mics.append(self.input_files[k].blocks(
                blocksize=self.unit_conf['frame_len'],
                overlap=0,
                frames=self.duration,
                dtype=self.unit_conf['dtype']))
    
        if self.app_conf['output']['store']:
            self.output_files = io.open_output_files(
                source_path=self.output_dir,
                unit_config={k: self.unit_conf[k] for k in parsubset},
                app_config=self.app_conf['output'])
            self.recs = self.output_files
        else:
            self.recs = None

        return None

    def _io_terminate(self) -> None:
        [f.close for f in self.input_files]
        if self.app_conf['output']['store']:        
            [f.close for f in self.output_files]
        return None

    def run(self) -> None:
        
        for n, m_frame in enumerate(zip(*self.mics)):
            self.unit.cycle(data=np.stack(m_frame, axis=0))
            if self.app_conf['output']['store']:
                for k in range(self.app_conf['output']['nchan']):
                    self.recs[k].write(self.unit.out[k,:])
            if n == self.nframes:
                break

        self._io_terminate()

        return None

class DeviceIOProcessingApp(App):
    def __init__(self):
        super().__init__(config_file=config_file)

    def io_init(self) -> None:
        pass

    def run(self) -> None:
        pass
