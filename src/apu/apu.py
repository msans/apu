from abc import ABC, abstractmethod
import numpy as np
import apu.utils.configurator as configurator

class Unit(ABC):
    def __init__(self):
        pass

    @abstractmethod
    def cycle(self, frame_in=np.array) -> None:
        pass
