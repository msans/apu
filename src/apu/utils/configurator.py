import pathlib
import tomli

def walk(filecontent_config=dict,config=dict) -> None:

    for key, item in filecontent_config.items():
        if isinstance(item,dict):
            k = '_sub_' + key
            config[k] = dict()
            walk(item,config[k])
        elif key == '_config' and isinstance(item, list):  # sub-module config file
            get_conf(item, config)
        else:
           config[key] = item  # key,value parametrization

    return None

def get_conf(path_parts=list, config=dict):
    filecontent_config = read_file(pathlib.Path(*path_parts))
    walk(filecontent_config=filecontent_config,config=config)

class ConfigReader():
    def __init__(self, conffiles_path: pathlib.Path):
        self.config = dict()
        get_conf(conffiles_path.parts, config=self.config)

    def get(self) -> dict:
        return self.config

def read_file(config_file=pathlib.Path) -> dict():
    try:
        with open(config_file, 'rb') as f:
            conf = tomli.load(f)
    except (IOError, tomli.TOMLDecodeError) as err:
        print(f"ERROR: could not read config file {config_file}: ", err)
        raise
    return conf

def read_conf(config_file=str) -> dict():
    return read_file(pathlib.Path(config_file))

def get_path(path_file=list) -> pathlib.Path:
    p = read_file(pathlib.Path(*path_file))
    return pathlib.Path(*p['path'])
