
import pathlib
import math
#import librosa
#import wavfile
import numpy as np
import soundfile as sf

# audio file

class ApuInputError(Exception):
    def __init__(self, message):            
        # Call the base class constructor with the parameters it needs
        super().__init__(message)

#def open_input_sources():
#   pointer over open_input_files and open_input_devices

def open_output_files(
        source_path=pathlib.Path,
        unit_config=dict,
        app_config=dict) -> list:

    output_files = list()
    frame_len = unit_config['frame_len']
    sampling_rate = unit_config['sampling_rate']
    for n in range(app_config['nchan']):
        p = pathlib.Path(
            source_path,
            app_config['file_prefix']+f"{n}.{app_config['format']}")
        f = sf.SoundFile(
            str(p),
            'w',
            channels=1,
            samplerate=sampling_rate,
            subtype=app_config['subtype'])
        output_files.append(f)

    return output_files

def open_input_files(
        source_path=pathlib.Path,
        unit_config=dict,
        app_config=dict) -> (list, int):

    input_files = list()
    durations = list()
    frame_len = unit_config['frame_len']
    sampling_rate = unit_config['sampling_rate']
    for n in range(app_config['nchan']):
        p = pathlib.Path(
            source_path,
            app_config['file_prefix']+f"{n}.{app_config['format']}")
        f = sf.SoundFile(str(p),'r')
        nchan = f.channels
        sr = f.samplerate
        nsamples = f.frames # assumed to be mono: 1 sample/frame
        
        if 'subtype' in app_config: 
            if app_config['subtype'] != f.subtype:
                f.close()
                [openedf.close for openedf in input_files]
                raise ApuInputError(" ".join([
                    f"Input file: Expected {app_config['subtype']}",
                    f", found {f.subtype}: {p}"]))

        if nchan != 1: 
            f.close()
            [openedf.close for openedf in input_files]
            raise ApuInputError(" ".join([
                f"Input file: Expected 1 channel",
                f", found {nchan}: {p}"]))
            
        if sr != sampling_rate: 
            f.close()
            [openedf.close for openedf in input_files]
            raise ApuInputError(" ".join([
                f"Input file: Expected sampling rate {sampling_rate}Hz",
                f", found {sr}Hz: {p}"]))

        min_len = 2*frame_len
        if nsamples <= min_len: 
            f.close()
            [openedf.close for openedf in input_files]
            raise ApuInputError(" ".join([
                f"Input file: Expected > {min_len}[sample]",
                f", found {nsamples}[sample]: {p}"]))
        
        input_files.append(f)
        durations.append(nsamples)

    nframes = int(np.floor(np.min(durations) /frame_len)) 

    return input_files, nframes
